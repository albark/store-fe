import Cookies from "universal-cookie"

export default function getUser() {
    let cookies = new Cookies()
    let user = cookies.get('user')
    return user
  }