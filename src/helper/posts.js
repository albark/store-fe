import axios from 'axios';

export default async function Post(endpoint, data) {
    return await axios.post(`http://localhost:5002/${endpoint}`, {data});
}
