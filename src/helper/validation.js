function lengthInvalid(input) {
    if(input.length <= 0 || input.length > 100 ) {
      return "Input must be between 0 and 100 characters long.";
    }
    return false;
}
  
  function passwordLengthInvalid(input) {
    if(input.length < 10) {
      return "Must be an alpha numeric at least 10 characters long.";
    }
    return false;
  }
  
  function emailInvalid(input) {
    const emailRE = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
    if(!emailRE.test(input)) {
      return "Must be a valid email address.";
    }
    return false;
  }
  function addressInvalid(input) {
    const emailRE = /^[#.0-9a-zA-Z\s,-]+$/;
    if(!emailRE.test(input)) {
      return "Must be a valid street address.";
    }
    return false;
  }
  
  function addressline2Invalid(input) {
    // const emailRE = /\S+@\S+\.\S+/;
    // if(!emailRE.test(input)) {
    //   return "Must be a valid street address.";
    // }
    return false;
  }
  function cityInvalid(input) {
    // const emailRE = /^[#.0-9a-zA-Z\s,-]+$/;
    // if(!emailRE.test(input)) {
    //   return "Must be a valid city address.";
    // }
    return false;
  }
  function zipInvalid(input) {
    // const emailRE = /^[#.0-9a-zA-Z\s,-]+$/;
    // if(!emailRE.test(input)) {
    //   return "Must be a valid city address.";
    // }
    return false;
  }
  function countryInvalid(input) {
    if(input != "US") {
      return "Must be a valid country.";
    }
    return false;
  }
  function stateInvalid(input) {
    try {
  
    input = input.toLower();
    let states = [
      'alabama',
      'al',
      'alaska',
      'ak',
      'arizona',
      'az',
      'arkansas',
      'ar',
      'california',
      'ca',
      'colorado',
      'co',
      'connecticut',
      'ct',
      'delaware',
      'de',
      'florida',
      'fl',
      'georgia',
      'ga',
      'hawaii',
      'hi',
      'idaho',
      'id',
      'illinois',
      'il',
      'indiana',
      'in',
      'iowa',
      'ia',
      'kansas',
      'ks',
      'kentucky',
      'ky',
      'louisiana',
      'la',
      'maine',
      'me',
      'maryland',
      'md',
      'massachusetts',
      'ma',
      'michigan',
      'mi',
      'minnesota',
      'mn',
      'mississippi',
      'ms',
      'missouri',
      'mo',
      'montana',
      'mt',
      'nebraska',
      'ne',
      'nevada',
      'nv',
      'new hampshire',
      'nh',
      'new jersey',
      'nj',
      'new mexico',
      'nm',
      'new york',
      'ny',
      'north carolina',
      'nc',
      'north dakota',
      'nd',
      'ohio',
      'oh',
      'oklahoma',
      'ok',
      'oregon',
      'or',
      'pennsylvania',
      'pa',
      'rhode island',
      'ri',
      'couth carolina',
      'sc',
      'south dakota',
      'sd',
      'tennessee',
      'tn',
      'texas',
      'tx',
      'utah',
      'ut',
      'vermont',
      'vt',
      'virginia',
      'va',
      'washington',
      'wa',
      'west virginia',
      'wv',
      'wisconsin',
      'wi',
      'wyoming',
      'wy',
      'american samoa',
      'as',
      'district of Columbia',
      'dc',
      'federated states of micronesia',
      'micronesia',
      'fm',
      'guam',
      'gu',
      'marshall islands',
      'mh',
      'northern mariana islands',
      'mp',
      'palau',
      'pw',
      'puerto rico',
      'pr',
      'virgin islands',
      'vi',
    ]
    if (states.indexOf(input) < 0) {
        return "Must be a valid state.";
    }
    return false;
    } catch(e) {
      return false;
    }
  }
  
  function nameInvalid(input) {
    const emailRE = /^[a-zA-Z]+ [a-zA-Z]+$/;
    if(!emailRE.test(input)) {
      // return "Must be a valid first and last name.";
      return false;
    }
    return false;
  }
  //todo fix
//   function priceInvalid(input) {
//     input = input.replace(/[^\d.-]/g, '');
//     input = parseFloat(input(price).input(2)) * 100;
//     if(parseInt(input) > 0) {
//       return "Price must be greater than $0.00";
//     }
//     return false;
//   }
  
  function numberGreaterThan0Invalid(input) {
    if(parseInt(input) > 0) {
      return "Must be greater 0";
    }
    return false;
  }
  
  function isInvalidCheck(input, checkType) {
    if(checkType === "lengthInvalid") {
      return lengthInvalid(input);
    }
    if(checkType === 'passwordLengthInvalid') {
      return passwordLengthInvalid(input);
    }
    if(checkType === 'emailInvalid') {
      return emailInvalid(input);
    }
    if(checkType === 'addressInvalid') {
      return addressInvalid(input);
    }
    if(checkType === 'addressline2Invalid') {
      return addressline2Invalid(input);
    }
    if(checkType === 'cityInvalid') {
      return cityInvalid(input);
    }
    if(checkType === 'countryInvalid') {
      return countryInvalid(input);
    }
    if(checkType === 'stateInvalid') {
      return stateInvalid(input);
    }
    if(checkType === 'nameInvalid') {
      return nameInvalid(input);
    }
    if(checkType === 'zipInvalid') {
      return zipInvalid(input);
    }
  
    console.log('here..')
    throw new Error("Invalid checkType passed to isInvalid:", checkType);
  }
  
  // this is being used on the click event to check if form is valid
  function checkIsValid(that, state) {
    let isValid = true;
    for(let statehandler of Object.keys(state.checkValid)) {
      let checkType = state.checkValid[statehandler].checktype;
      let isInvalid = isInvalidCheck(state[statehandler], checkType)
      if( isInvalid ){
        let obj = state
        obj.checkValid[statehandler] = {...obj.checkValid[statehandler], errors: isInvalid, unvalidated: false};
        that.setState(obj)
        isValid = false;
      }
    }
    return isValid;
  }
  
  
function checkValid(value, checkType) {
    let isInvalid = isInvalidCheck(value, checkType)
    return isInvalid;//{errors: isInvalid, unvalidated: false}
}
    
  export {
    checkValid,
    checkIsValid,
  }
  