import axios from 'axios';

export default async function Get(endpoint, data) {
    return await axios.get(`http://localhost:5002/${endpoint}`, {
        params: {
            data
        }
    });
}
