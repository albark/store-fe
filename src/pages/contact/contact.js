import './styles.css';
import React, { useState }from 'react';
import Post from '../../helper/posts';
import Footer from '../../components/Footer'

let senderEmail = React.createRef();
let message = React.createRef();

const states = {
  LOADING: 'LOADING',
  SUCCESS: 'SUCCESS',
  ERROR: 'ERROR'
}

async function sendContactUs(msg, setMsg) {

  setMsg({...msg, state: 'LOADING'})
  
  if(!senderEmail.current.value) {
    setMsg({
      ...msg,
      state: states.ERROR,
      data: '',
      error: 'Not a valid email'
    })
  }

  if(!message.current.value) {
    setMsg({
      ...msg,
      state: states.ERROR,
      data: '',
      error: 'Not a valid message'
    })
  }
  
  if (senderEmail.current.value && message.current.value) {
    try {
      const result = await Post('contact', {
        senderEmail: senderEmail.current.value,
        message: message.current.value
      });
      setMsg({
        ...msg,
        state: states.SUCCESS,
        data: result.data,
        error: ''
      })
    } catch (error) {
      setMsg({
        ...msg,
        state: states.ERROR,
        data: '',
        error: 'Failed To Send Message'
      })
    }
  }
}

export default function Contact() {
  const [msg, setMsg] = useState({
    state: '',
    error: '',
    data: [],
  })

  return (
    <div className="root">
      <div className="container">
        <h1>Contact Us</h1>
        <div>
          {msg.state == states.LOADING &&
            <h3>LOADING</h3>
          }

          {msg.state === states.SUCCESS &&
            <div>
              <h3>Message Sent!</h3>
              <p>We'll get back to you as soon as possible.</p>
            </div>
          }

          {msg.state === states.ERROR &&
            <div>
              <h3>Error Sending Message</h3>
              <p>{msg.error}</p>
            </div>
          }
          <div>
            <label>Email</label>
            <input
              id="email"
              className="message"
              type="text"
              name="email"
              ref={senderEmail}
              placeholder="your@email.com" />
          </div>
    
          <div>
            <textarea
              className="message"
              rows="5"
              cols="80"
              id="message"
              ref={message}
              placeholder="How can we help you?">
            </textarea>
          </div>
          <div>
            <button onClick={() => sendContactUs(msg, setMsg)}>
              Send Message
            </button>
          </div>
        </div>      

    </div>
    <Footer />
  </div>
  );
}