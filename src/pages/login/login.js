import React from 'react';
import Cookies from 'universal-cookie';
import Footer from '../../components/Footer'
import Header from '../../components/Header'
import Post from '../../helper/posts';
import './styles.css';
import { useNavigate } from 'react-router-dom';

let email = React.createRef();
let password = React.createRef();

// async function updateCookie() {
//   const cookies = new Cookies();
//   let cart_owner_id = cookies.get('cart_owner_id');
  
//   if( cart_owner_id ) {
//     Post('mergeShoppingCart', {owner: userId, cartOwnerId: cart_owner_id.ownerId})
//     cookies.remove('cart_owner_id');
//   }

//   cookies.set('cart_owner_id', {
//     ownerId: userId
//   });
// }

async function handleSubmit(history) {

  let cookies = new Cookies()
  const result = await Post('login', {
    email: email.current.value,
    password: password.current.value
  });
  console.log('result', result)
  let {token, user} = result.data; 

  cookies.set('login_token', token);
  cookies.set('user', user);
  
  // updateCookie(user.id)

  // //todo finish
  let queryParam = 'shop'
  if(queryParam == "checkout") {
    // history.push('/checkout');
  } else {
    history.push('/shop');
  }
}

export default function Login() {
  const history = useNavigate();

  return (
    <div>
      <Header />
        <div className="container">
          <h1>Login</h1>
          <p className="lead">Log in with your email address.</p>
            <div className="formGroup">
              <label className="label" htmlFor="email">
                email address:
              </label>
              <input
                className="input"
                id="email"
                type="text"
                name="email"
                ref={email}
                autoFocus
              />
            </div>
            <div className="formGroup">
              <label className="label" htmlFor="password">
                Password:
              </label>
              <input
                className="input"
                id="password"
                type="password"
                name="password"
                ref={password}
              />
            </div>
            <div className="formGroup">

              <button className="button" onClick={async () => {await handleSubmit(history)}} >
                Log in
              </button>

            </div>
        </div>
      <Footer />
    </div>
  );
}
