import Footer from '../../components/Footer'
import Header from '../../components/Header'

import './styles.css';

export default function About() {
  return (
    <div>
      <Header />
        <div className='about'>
          <h1>About Us</h1>
          <p>Welcome to Freedom Frenchie,, we are delighted to have you here!</p><p>Freedom Frenchie brings a passion and love for french bulldogs.</p>

          <p>Alex and Nea have been lifelong dog lovers, with Alex spending his early years helping to train huskys for the iditarod in the northern part of Michigan, and Nea donating her time to help and train shelter pits.</p>
          <p>For the longest time we thought we were only ever going to be 'big-dog' owners, but that all changed when we rescured our first french bulldog, Skye, our brindled baby. Despite her small stature, her huge personality had us forever enamured with the beauty of the breed.</p>

          <p>After a tour of duty through the middle east, we found a new mission in both raising skye and spreading the love of the fabulous french bulldog. We highly appreciate your patronage of Freedom Frenchie!</p>
        </div>
      <Footer />
    </div>
  );
}