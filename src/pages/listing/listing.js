import React, { useState }from 'react';
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import { Link, useNavigate, useParams } from 'react-router-dom';
import Get from '../../helper/gets';
import Cookies from 'universal-cookie';

import './styles.css';
import Post from '../../helper/posts';
import getUser from '../../helper/getUser';
const states = {
  LOADING: 'LOADING',
  SUCCESS: 'SUCCESS',
  ERROR: 'ERROR'
}




async function getListing(listingid, listing, setListing, setSelectedImage) {
  console.log('listing', listingid)
  setListing({...listing, state: 'LOADING'})

  try {
    // const result = await Get('listing', listingid);
    const result = {
      title: 'foo',
      images: 'AAA00352,AAA00345,AAA00350,AAA00318,swatches-labeled',
      price: 1234,
      description: ['123','234'],
    }
    console.log('listing', result)
    setListing({
      ...listing,
      state: states.SUCCESS,
      // data: result.data.listing,
      data: result, 
      error: ''
    })
    // let initialImg = result.data.listing.images.split(',')[0]
    const initialImg = result.images.split(',')[0]
    console.log('initalImg', initialImg)
    setSelectedImage({
      state: 'SUCCESS',
      error: '',
      data: initialImg
    })
  } catch (error) {
    
    setListing({
      ...listing,
      state: states.ERROR,
      data: [],
      error: 'Failed To Get Listings'
    })
  }
}

function buildImageViewerSelector(images, setSelectedImage) {
  images = images.split(',')
  let viewer = images.map((item,index) => {
    return <div key={images[index].trim()} className='two columns itemCardImageContainer'>
      <img src={`../item-listings/${images[index].trim()}.jpg`}
        onClick={()=>{setSelectedImage({
          state: 'SUCCESS',
          error: '',
          data: images[index]
        } )}}
        className={`itemCardImage ${index == 0 ? 'selectedImage' : ''}`} />
    </div>
  });
  return viewer
}

export default function Listing() {
  const history = useNavigate();
  const listingid  = useParams()
  
  async function AddToCart(listing) { 
    const cookies = new Cookies();
    let cart = cookies.get('cart')
    let updatedCart;
    if(cart) {
      let foundItem = false
      updatedCart = cart.map((item)=>{
        if(item.listingId == listing.data.id) {
          foundItem = true
          item.quantity = parseInt(item.quantity) + 1
        }
        return item
      })
      if(foundItem == false) {
        cart.push({
          images: listing.data.images,
          title: listing.data.title,
          price: listing.data.price,
          listingId: listing.data.id,
          quantity: 1,          
        })
        updatedCart = cart;
      }
    }
    if(updatedCart){
      cookies.set('cart', updatedCart)
    }
    if(!cart) {
      cookies.set('cart', [
        {
          images: listing.data.images,
          title: listing.data.title,
          price: listing.data.price,
          listingId: listing.data.id,
          quantity: 1,
        }
      ])
    }

    updatedCart = cookies.get('cart')
    let user = cookies.get('user')
    let loginToken = cookies.get('login_token')
    if(user && loginToken) {
      const result = await Post('updateCart', {
        user: {email: user.email},
        loginToken: loginToken,
        cart: updatedCart
      })
    }

    
    history.push('/cart');
  
  }
  
  const [listing, setListing] = useState({
    state: 'INITIAL',
    error: '',
    data: undefined
  })

  const [selectedImage, setSelectedImage] = useState({
    state: 'INITIAL',
    error: '',
    data: undefined,
  })

  if(listing.state === 'INITIAL') {
    getListing(listingid, listing, setListing, setSelectedImage)
  }
  let imageViewerSelector;
  if(listing.data) {
    console.log('listingdata', listing.data)
  imageViewerSelector = buildImageViewerSelector(listing.data.images,setSelectedImage)
  }
  return (
    <div className='root'>
      <Header />
      <script async
        src="https://js.stripe.com/v3/buy-button.js">
      </script>
    {(listing.state == 'SUCCESS' && selectedImage.state == 'SUCCESS') &&
      <div className='container'>
            <div className='row'>
            &nbsp;
            </div>
            <div className='row selectedItemCardImageRowContainer'>
              <div className='eight columns selectedItemCardImageContainer'>
                <img src={`../item-listings/${(selectedImage.data).trim()}.jpg`} className='selectedItemCardImage' />
              </div>
              <div className='four columns'>
                <div className='row center'>
                  <h3>{listing.data.title}</h3>
                </div>

                <div className='row center'>
                  <h4>${listing.data.price / 100}</h4>
                </div>

                <div className='row center'>
                  {/* <button onClick={()=>{AddToCart(listing);
                
                }}>Add To Cart</button> */}
                      <stripe-buy-button
        buy-button-id="buy_btn_1PyAMiDS5DTAdtukrZjpWZfS"
        publishable-key="pk_test_sUIqkpbsGEGmkxFPSP5b4dlt00sIVFCiTG"
      >
      </stripe-buy-button>
                </div>
                <hr />

                <div className='row'>
                  <p>{listing.data.description}</p>
                    {/* {listing.data.description.map((descriptionParagraph,index) =>
                      <p key={index}>{descriptionParagraph}</p>
                    )} */}
                </div>

              </div>
            </div>
            <div className='row imageViewerImages'>
              {imageViewerSelector}
            </div>
            <div className='row'>
                &nbsp;
            </div>
          </div>
      }

      <Footer />
    </div>
  );
}