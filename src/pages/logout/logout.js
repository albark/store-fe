import Header from '../../components/Header'
import Footer from '../../components/Footer'
import { useNavigate } from "react-router-dom";
import './styles.css';

import Cookies from 'universal-cookie';

export default function Logout() {
  let cookies = new Cookies()
  let history = useNavigate()

  
  cookies.remove("user")
  cookies.remove("login_token")
  cookies.remove("cart")
  cookies.remove("client_secret")
  cookies.remove("customer_paymentmethods")
  cookies.remove("__stripe_sid")
  cookies.remove("__stripe_mid")

  history.push('/login')
  return (
    <div>
      <Header />
      <Footer />
    </div>
  );
}