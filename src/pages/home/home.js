import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

import Footer from '../../components/Footer'
import './styles.css';


function Explore () {
  return <div className="actionMainContainer">
  <div className="container actionContainer">
    <div className="row subTagline">
      <h2>Explore</h2>
    </div>

    <div className="row subTaglineTwo">
      <p>
        {this.props.homepageCopy.subTaglineTwo}
      </p>
    </div>

    <div className="row actionButtons">
      {/* <ExploreCard cards={this.props.cards}/> */}
    </div>
  </div>
</div>
}

function Header () {
  return (
    <div className="root">
      <script data-ad-client={"googleTrackingId"} async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
      <div className="container">
        <Link className="brand" to="/">
          <img
            src={"/"}
            width="32"
            height="32"
            alt="React"
            className="brandedURL"
          />
          <span className="brandTxt">Freedom Frenchie</span>
        </Link>
      </div>
    </div>
  );
}

function Pricing () {
    return <div className="pricingContainer">
      <div className="container actionContainer">
        <div className="row subTagline">
          <h2>Stay In Style</h2>
        </div>

        <div className="row subTaglineTwo">
          <p>
          Collections added regularly to keep your world fresh and interesting.
          </p>
        </div>
      </div>
    </div>
}



function CallToAction() {
  return <div className="callToActionContainer">
  <div className="container actionContainer">
    <div className="row subTagline">
      <h2>View Latest Collection</h2>
    </div>
    <div className="row subTaglineTwo">
      <p>Subscribe to our newsletter to be informed of releases, flash drops, sales, and more.</p>
      <p>
      <button
          onClick={(e)=>this.handleClick(e,'tier1')}
          className="actionButtonsButton buyingButton">
            Start Now
          </button>
      </p>
    </div>
  </div>
</div>
}




// export default function Home() {
//   return C
// }

export default function Home() {
  const [count, setCount, data, setData] = useState(0);
  // Similar to componentDidMount and componentDidUpdate:  
// useEffect(async() => {    // Update the document title using the browser API
//   document.title = `You clicked ${count} times`;
//   const result = await axios(
//     'https://hn.algolia.com/api/v1/search?query=redux',
//   );

// });

useEffect(() => {
  async function fetchData() {
    try {
      const result = await axios(
        'https://hn.algolia.com/api/v1/search?query=redux'
      );
      setData(result.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  }

  fetchData();

  // Update the document title using the browser API
  document.title = `You clicked ${count} times`;

}, [count]); // The effect depends on `count`, so it re-runs when `count` changes


let projectName = 'test';//config.projectName;
  const copy = 'test'//config[projectName];
  let user = {}//GetUser(this.props.req);
  return (
    <div className="root">
      <div className="loginHeaderContainer">
        {/* <Header user={user} copy={copy} /> */}
        {/* <div className={s.loginHeader}>
        <Link className={s.link} to="/register">
          Signup
        </Link>
        <Link className={s.link} to="/login">
          Login  background-image: url('../../../public/myceliadesigns/a4.png');

        </Link>
        </div> */}
      </div>
      <div className="curvedSection"
        style={{
          backgroundImage: "url(" + `./splash.png` + ")",
          backgroundPosition: 'center',
          backgroundSize: 'cover',
          backgroundRepeat: 'no-repeat'
        }}
      >
        <div className="container taglineContainer">
          <div className="row taglines">
            <h1 className="topTagline">Stunning Designs For The Modern Person</h1>
            <p className="bottomTagline">
            Shop our work and order something to make daily life absolutely pop with color.
            </p>
          </div>
          <div className="actionButtons taglineActionButtons">
            {/* <button
            onClick={(e)=>this.handleClick(e,'tier1')}
            className={`${s.actionButtonsButton} ${s.buyingButton}`}>
              Shop Now
            </button> */}
            <div>

            <Link to="/shop"><button>Shop</button></Link>
            </div>
            <div>
            <a href="https://www.redbubble.com/people/myceliadesigns/shop?asc=u"><button>Shop Collection</button></a>

            </div>
          </div>
        </div>
      </div>


      {/* <Explore homepageCopy={this.props.copy.homepageCopy} cards={this.props.exploreCards} /> */}

      <div className="normalCurve" />
      <div className="curvedSectionTwo">
      <Pricing homepageCopy={"FOO"} />
      </div>

      <div>

      <CallToAction homepageCopy={"homepagecopy"} />

      </div>
      <div className="curvedSectionDarker">
       <Footer copy={"footer"} />
      </div>
    </div>
  );
}