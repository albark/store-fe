import React, {useState} from 'react';
import { useNavigate } from "react-router-dom";
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import Cookies from 'universal-cookie';

import './styles.css';
import Post from '../../helper/posts';

export default function Cart() {
  const [quantity, setQuantity] = useState(1);
  const [cartItemCount, setCartItemCount] = useState(countItemsInCart())

  const history = useNavigate();

  function handleCheckoutClick(cartShop) {
    history.push('/checkout');
  }
  function getCart() {
    const cookies = new Cookies();
    let cart = cookies.get('cart')
    console.log('get', cart)
    return cart;
  }
  
  function countItemsInCart(){
    const cookies = new Cookies();
    let cart = cookies.get('cart')
    console.log('cart-----', cart)

    let itemCount = 0
    if(cart) {
      cart.map((item) => {
        itemCount += parseInt(item.quantity)
      });
    }
    return itemCount
  }

  async function updateQuantity(listingId, quantity) {
    const cookies = new Cookies();
    console.log('id', listingId, quantity)
    let cart = getCart()
    cart = cart.map((item) => {
        if(item.listingId == listingId) { 
            item.quantity = quantity
        }
        return item
    })

    let cartLength = cart.length
    let itemToDelete;
    for (let i = 0; i < cartLength; i++) {
      if(parseInt(cart[i].quantity) == 0) {
        itemToDelete = cart.splice(i,1)
      }     
    }
    
    cookies.set('cart', cart)

    let updatedCart = cookies.get('cart')
    let user = cookies.get('user')
    let loginToken = cookies.get('login_token')
    if(user && loginToken) {
      if(itemToDelete) {
        const deleteResult = await Post('deleteCartItem', {
          user: {email: user.email},
          loginToken: loginToken,
          cartItem: itemToDelete[0]
        })
      }

      const result = await Post('updateCart', {
        user: {email: user.email},
        loginToken: loginToken,
        cart: updatedCart
      })
    }
  }

  function handleSelectChange(e) {
    let listingId = e.target.name
    let quantity = e.target.value
    console.log('quantity', quantity)
    setQuantity(parseInt(quantity))
    updateQuantity(listingId, quantity)
    setCartItemCount(countItemsInCart())
  }

  
  function CartElement() {
    let cart = getCart() 
    console.log('cart',cart)
    let cartCheckout = <div></div>;
    if(cart) {

      cartCheckout = (cart).map((listing, indx) => (
      <div className="Cart" key={indx}>
          <div className='row cartListing'>
            <div className='three columns'>
              <img className='cartCheckoutImg'
              src={`./item-listings/${listing.images.split(',')[0]}.jpg`}/>
              </div>
            <div className='six columns listingText'>
            <p>{listing.title}</p>
            <p>${(listing.price/100).toFixed(2)}</p>

            <div className='row'>
                <p>Quantity</p>
                <select 
                  name={listing.listingId}
                  //  value={quantity}
                  defaultValue={listing.quantity}
                  onChange={handleSelectChange}>

                    {[...Array(parseInt(25)).keys()].map((number) =>
                        <option value={number}>{number}</option>
                    )}
                </select>
            </div>

            </div>
            &nbsp;
          </div>
      </div>
    ));
  }
  return cartCheckout
}
  
  return (

    <div>
      <Header />
      {cartItemCount > 0 ?
        <div>
        {cartItemCount == 1 ?
            <div className="container">
            <h3>{cartItemCount} item in your cart</h3>         
              <CartElement />
              <button onClick={handleCheckoutClick}>Checkout</button>

            </div>
            :
            <div className="container">
              <h3>{cartItemCount} items in your cart</h3>
              <CartElement />
              <button onClick={handleCheckoutClick}>Checkout</button>

          </div>
          }</div>
          :<div className="container">

            <div>There are no items in your cart yet. Go <a href="/shop">shop</a>!</div>
          </div>
        }
      <Footer />
    </div>
  );
}
