import Header from '../../components/Header'
import Footer from '../../components/Footer'

import './styles.css';
export default function Blog() {
  return (
    <div>
      <Header />
      <Footer />
    </div>
  );
}