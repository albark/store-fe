import React, { useState }from 'react';
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import {Link} from 'react-router-dom';
import Get from '../../helper/gets';
import './styles.css';
const states = {
  LOADING: 'LOADING',
  SUCCESS: 'SUCCESS',
  ERROR: 'ERROR'
}

async function getListings(listings, setListings) {

  setListings({...listings, state: 'LOADING'})

  try {
    // const result = await Get('listings');
    const result = [
      {
        title: 'Instamax Picture Frame Holder',
        images: 'AAA00352',
        price: 1234
    },
    {
      title: 'Frenchie Keychain',
      images: 'AAA00352',
      price: 1234
  },
  {
    title: 'Instamax Printing Servies',
    images: 'AAA00352',
    price: 1234
},

  ]
    // console.log('result', result)
    setListings({
      ...listings,
      state: states.SUCCESS,
      // data: result.data.listings,
      data: result,
      error: ''
    })
  } catch (error) {
    setListings({
      ...listings,
      state: states.ERROR,
      data: [],
      error: 'Failed To Get Listings'
    })
  }
}

export default function Marketplace() {
  
  const [listings, setListings] = useState({
    state: 'INITIAL',
    error: '',
    data: [],
  })
  if(listings.state === 'INITIAL') {
    getListings(listings, setListings)
  }
    let listingComponent = listings.data.map((listing,index) => (
      
      <div className={`three columns ${index % 4 == 0 ? 'newRow' : 'f'}` }>
          <div className="SaleItemCard">
        <Link className="link" to={`/listing/${listing.id}`}>

            <div className="row itemCardImageRowContainer">
              <div className="tweleve columns itemCardImageContainer">
                <img src={`./item-listings/${listing.images.split(',')[0]}.jpg`} className="itemCardImage" />
              </div>
            </div>
            <div className="itemDataContainer">
            <div className="row">
              <div className="tweleve columns">
                <p className="itemCardTitle">{listing.title}</p>
              </div>
            </div>
            <div className="row">
              <div className="tweleve columns">
                <p>${listing.price / 100}</p>
              </div>
            </div>

            <div className="row">
            </div>
            </div>
          </Link>
        </div>
      </div>
    ));

    return (
      <div className="root">
        <Header />

      <div className="container">

        <h1>Marketplace</h1>
      </div>
        <div className="container">
          {listingComponent}
        </div>
        <Footer />
      </div>
    );
}