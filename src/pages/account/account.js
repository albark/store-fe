import React, { useState }from 'react';

import Footer from '../../components/Footer'
import Header from '../../components/Header'
import Get from '../../helper/gets';

import Cookies from 'universal-cookie';
import { Link } from 'react-router-dom';


import './styles.css';
const states = {
  LOADING: 'LOADING',
  SUCCESS: 'SUCCESS',
  ERROR: 'ERROR'
}
async function getOrders(user, orders, setOrders) {

  setOrders({...orders, state: 'LOADING'})
  console.log('user', user)
  try {
    const result = await Get('orders', user);
    console.log('resu;tytt', result)
    setOrders({
      ...orders,
      state: states.SUCCESS,
      data: result.data.orders,
      error: ''
    })
  } catch (error) {
    setOrders({
      ...orders,
      state: states.ERROR,
      data: [],
      error: 'Failed To Get Orders'
    })
  }
}

export default function Account() {
  const [orders, setOrders] = useState({
    state: 'INITIAL',
    error: '',
    data: [],
  })
  const cookies = new Cookies();
  const user = cookies.get('user');
  // const returns = [] // todo

  if(orders.state === 'INITIAL') {
    getOrders(user, orders, setOrders)
  }

  function getOrderComponent(orders) {
    let orderComponent;
    orders = orders.data.reverse();
    console.log('orders.....',orders)
    if (orders && orders.length > 0) {
 
      //  ${JSON.parse(order.listingIds).(order.images).split(',')[0];
      // you are here. need to create the orders route with the order id
      // also then need to load all the info for each listing in the order
      orderComponent = orders.map(order => (
        <div className="row">
        <Link className="link eight columns" to={`/sale_orders?orderid=${order.id}`}>
        <div className="order" key={order.id}>
        <div className="row">
        <div className="two columns">
          {/* <img className="orderIcon" src={`./user-uploaded/${JSON.parse(order.listingIds)[0][1].images.split(',')[0]}.jpg`} /> */}
        </div>
          <div className="six columns">
            {/* <p>{`${JSON.parse(order.listingIds)[0][1].title}`}</p> */}
          </div>
        </div>
          <div className="row">
            <div className="six columns">
              <p>Date Ordered</p>
            </div>
            <div className="six columns">
              <p>{new Date(parseInt(order.date_ordered)).toLocaleDateString("en-US").toString()}</p>
            </div>
          </div>
          <div className="row">
            <div className="six columns">
              {/* <p>Quantity</p> */}
            </div>
            <div className="six columns">
              {/* <p>{`${JSON.parse(order.listingIds)[0][0]}`}</p> */}
            </div>
          </div>
  
          <div className="row">
            <div className="six columns">
              <p>Order Total</p>
            </div>
            <div className="six columns">
              <p>${order.order_cost / 100 || 15345 / 100}</p>
            </div>
          </div>
  
  
        </div>
        </Link>
        <div className="four columns">
        <div>
          <p>{order.status || ''}</p>
        </div>
        {/* <a href={`https://duckduckgo.com/?q=${order.trackingid || 'no id'}&t=canonical&ia=answer`} target="_blank">
          <button>
            Track Package
          </button>
        </a> */}
          <Link className="link" to="/contact">
            <button>
          Contact Seller
            </button>
        </Link>
        </div>
        </div>
      ));
    } else {
      orderComponent = 'You have not placed an order yet.';
    }
    return orderComponent
  }

  return (
    <div>
      <Header />
        <div className="root">
          <div className="container">
            <h1>Account</h1>
            <p>{(user && `Welcome Back ${user.email}!`) || 'Welcome Back!'}</p>
            <div>
            <h2>Orders</h2>
            {getOrderComponent(orders)}
          {/*  <button
              onClick={() => {
                history.push('orders_store');
              }}
            >
              View All Orders
            </button>*/}
          </div>
          </div>
        </div>
      <Footer />
    </div>
  );
}