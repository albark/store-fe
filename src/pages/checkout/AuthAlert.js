import React, { useState }from 'react';
import { checkValid } from '../../helper/validation'
import { useNavigate, useParams } from 'react-router-dom';

import Post from '../../helper/posts';
import Cookies from 'universal-cookie';

import './styles.css';
export default function AuthAlert(props) {

  let emailField = React.createRef();
  let passwordField = React.createRef();

  const [submitError, setSubmitError] = useState('')
  const [emailFieldErrors, setEmailFieldErrors] = useState('')
  const [passwordFieldErrors, setPasswordFieldErrors] = useState('')

  const [showRegister, setShowRegister] = useState(true)
  const [showLogin, setShowLogin] = useState(false)

  async function handleRegistration(props, special, history) {

    const emailInvalid = checkValid(emailField.current.value, 'emailInvalid')
    const passwordInvalid = checkValid(passwordField.current.value, 'lengthInvalid')
    console.log(emailInvalid, passwordInvalid)
    if ( emailInvalid || passwordInvalid ) {
      setSubmitError('Fix errors on page and try again')
      return;
    } else {
      setSubmitError('')
    }

    const result = await Post('register', {
      email: emailField.current.value,
      password: passwordField.current.value
    });
    let { loginToken, user, errors } = result.data;
    
    if(errors && errors.length >= 1) {
      this.setState({submitError: 'Error registering, if this problem persists please contact support'});     
      return;
    }
      const cookies = new Cookies();
      // let cart_owner_id = cookies.get('cart_owner_id');
      // if( cart_owner_id ) {
      //   let query =`mutation{mergeShoppingCart(
      //     owner:"${data.data.userRegister.id}",
      //     originalOwner: "${cart_owner_id.owner}"){
      //       id}}`;
      //   let result = await queryql(query);
      //   cookies.remove('cart_owner_id');
      // }


     cookies.set('login_token', loginToken);
     cookies.set('user', user);
     props.handleLoginBtnClick(special, history)
  }
  async function handleLogin(props, special, history) {
    const emailInvalid = checkValid(emailField.current.value, 'emailInvalid')
    const passwordInvalid = checkValid(emailField.current.value, 'lengthInvalid')
    console.log(emailInvalid, passwordInvalid)
    if ( emailInvalid || passwordInvalid ) {
      setSubmitError('Fix errors on page and try again')
      return;
    } else {
      setSubmitError('')
    }

    const result = await Post('login', {
      email: emailField.current.value,
      password: passwordField.current.value
    });
    let { loginToken, user, paymentMethods, errors } = result.data;
      console.log('errors', errors)
    if(errors && errors.length >= 1) {
      this.setState({submitError: 'Error logging in, if this problem persists please contact support'});     
      return;
    }
      const cookies = new Cookies();
      // let cart_owner_id = cookies.get('cart_owner_id');
      // if( cart_owner_id ) {
      //   let query =`mutation{mergeShoppingCart(
      //     owner:"${data.data.userRegister.id}",
      //     originalOwner: "${cart_owner_id.owner}"){
      //       id}}`;
      //   let result = await queryql(query);
      //   cookies.remove('cart_owner_id');
      // }

      console.log('===----===', user)
      console.log('0000000',paymentMethods)
    try {
      cookies.set('user', user);
      cookies.set('customer_paymentmethods', paymentMethods)
    } catch (error) {
      console.log(error)
    }
     

     cookies.set('login_token', loginToken);
     
     props.handleLoginBtnClick(special, history)
  }
  const history = useNavigate()
  const special = useParams()
  return (
    <div>
      {showRegister &&
        <div className="container">
          <h1>Register</h1>
            <div className="formGroup">
              <label className="label" htmlFor="email">
                Email address:
              </label>
              <input
                className={`
                input
                ${ emailFieldErrors ? `inputErrors`:''}
                `}
                id="email"
                type="email"
                name="email"
                placeholder="email"
                statehandler="email"
                checktype="emailInvalid"
                ref={emailField}
                onBlur={()=>{
                  setEmailFieldErrors(checkValid(emailField.current.value, 'emailInvalid'))}}
                onChange={()=>{
                  setEmailFieldErrors(checkValid(emailField.current.value, 'emailInvalid'))}}
              />
              { emailFieldErrors
                ? <p>{emailFieldErrors}</p>
                :<p>&nbsp;</p>}
            </div>
            <div className="formGroup">
              <label className="label" htmlFor="password">
                Password:
              </label>
              <input
                className={`
                  input
                  ${ passwordFieldErrors ? `inputErrors`:''}`}
                id="password"
                type="password"
                name="password"
                placeholder="password"
                statehandler="password"
                checktype="lengthInvalid"
                ref={passwordField}
                onBlur={()=>{
                  setPasswordFieldErrors(checkValid(passwordField.current.value, 'lengthInvalid'))}}
                onChange={()=>{
                  setPasswordFieldErrors(checkValid(passwordField.current.value, 'lengthInvalid'));}}
              />
            { passwordFieldErrors
             ? <p>{passwordFieldErrors}</p>
             :<p>&nbsp;</p>}

              <p onClick={()=>{setShowRegister(false);setShowLogin(true);}}>Already have an account? Log In</p>
             </div>
            <div className="formGroup">
              <button className="button" type="button" onClick={() => handleRegistration(props, special, history)}>
                Continue
              </button>
              <div>
                <p>{submitError}</p>
              </div>

            </div>
        </div>
    }
    {showLogin &&
        <div className="container">
        <h1>Login</h1>
          <div className="formGroup">
            <label className="label" htmlFor="email">
              Email address:
            </label>
            <input
              className={`
              input
              ${ emailFieldErrors ? `inputErrors`:''}
              `}
              id="email"
              type="email"
              name="email"
              placeholder="email"
              statehandler="email"
              checktype="emailInvalid"
              ref={emailField}
              onBlur={()=>{
                setEmailFieldErrors(checkValid(emailField.current.value, 'emailInvalid'))}}
              onChange={()=>{
                setEmailFieldErrors(checkValid(emailField.current.value, 'emailInvalid'))}}
            />
            { emailFieldErrors
              ? <p>{emailFieldErrors}</p>
              :<p>&nbsp;</p>}
          </div>
          <div className="formGroup">
            <label className="label" htmlFor="password">
              Password:
            </label>
            <input
              className={`
                input
                ${ passwordFieldErrors ? `inputErrors`:''}`}
              id="password"
              type="password"
              name="password"
              placeholder="password"
              statehandler="password"
              checktype="lengthInvalid"
              ref={passwordField}
              onBlur={()=>{
                setPasswordFieldErrors(checkValid(passwordField.current.value, 'lengthInvalid'))}}
              onChange={()=>{
                setPasswordFieldErrors(checkValid(passwordField.current.value, 'lengthInvalid'));}}
            />
          { passwordFieldErrors
           ? <p>{passwordFieldErrors}</p>
           :<p>&nbsp;</p>}
              <p onClick={()=>{setShowRegister(true);setShowLogin(false);}}>Don't have an account yet? Register now</p>

           </div>
          <div className="formGroup">
            <button className="button" type="button" onClick={() => handleLogin(props, special, history)}>
              Continue
            </button>
            <div>
              <p>{submitError}</p>
            </div>

          </div>
      </div>
    }
    </div>
  );
}