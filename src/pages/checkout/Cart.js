import React, {useState} from 'react';
import { useNavigate } from "react-router-dom";
import Cookies from 'universal-cookie';
import './styles.css';
import Post from '../../helper/posts';

export default function Cart() {
    const [cartSummedPrice, setCartSummedPrice] = useState(sumCartPrice())
    const [cartItemCount, setCartItemCount] = useState(countItemsInCart())
    const [quantity, setQuantity] = useState(1);
    const history = useNavigate();
  
    function countItemsInCart(){
        const cookies = new Cookies();
        let cart = cookies.get('cart')
        let itemCount = 0
        cart.map(item => {
            itemCount += parseInt(item.quantity)
        });
        return itemCount
    }
    function sumCartPrice() {
        const cookies = new Cookies();
        let cart = cookies.get('cart')
        let cartSum = 0
        cart.map(item => {
          cartSum += item.price * item.quantity
        });
    
        return cartSum
    }
    function GetOrderSummarySection() {
    
        let orderSummarySection = '';
        orderSummarySection = <div className="row orderSummary">
          <h3>Order Summary</h3>
          <div className="four columns">
            <p>Total</p>
          </div>
          <div className="four columns">
            ${(cartSummedPrice /100).toFixed(2)}
          </div>
        </div>
        return orderSummarySection;
    }

    function handleCheckoutClick(cartShop) {
        let x = quantity;
        console.log(x)
        history.push('/checkout');
    }
  
    function getCart() {
        const cookies = new Cookies();
        let cart = cookies.get('cart')
        return cart;
    }
  

    async function updateQuantity(listingId, quantity) {
      const cookies = new Cookies();
      console.log('id', listingId, quantity)
      let cart = getCart()
      cart = cart.map((item) => {
          if(item.listingId == listingId) { 
              item.quantity = quantity
          }
          return item
      })
  
      let cartLength = cart.length
      let itemToDelete;
      for (let i = 0; i < cartLength; i++) {
        if(parseInt(cart[i].quantity) == 0) {
          itemToDelete = cart.splice(i,1)
        }     
      }
      
      cookies.set('cart', cart)
  
      let updatedCart = cookies.get('cart')
      let user = cookies.get('user')
      let loginToken = cookies.get('login_token')
      if(user && loginToken) {
        if(itemToDelete) {
          const deleteResult = await Post('deleteCartItem', {
            user: {email: user.email},
            loginToken: loginToken,
            cartItem: itemToDelete[0]
          })
        }
  
        const result = await Post('updateCart', {
          user: {email: user.email},
          loginToken: loginToken,
          cart: updatedCart
        })
      }
    }

    function handleSelectChange(e) {
        let listingId = e.target.name
        let quantity = e.target.value

        setQuantity(quantity)

        updateQuantity(listingId, quantity)
        setCartItemCount(countItemsInCart())
        setCartSummedPrice(sumCartPrice())  
    }

  let cart = getCart()
  
  let cartCheckout = (cart).map((listing, indx) => (

     <div className="Cart" key={indx}>
         <div className='row cartListing'>
           <div className='three columns'>
             <img className='cartCheckoutImg'
             src={`./item-listings/${listing.images.split(',')[0]}.jpg`}/>
             </div>
           <div className='six columns listingText'>
           <p>{listing.title}</p>
           <p>${(listing.price/100).toFixed(2)}</p>

           <div className='row'>
               <p>Quantity</p>
               <select 
                 name={listing.id}
                //  value={quantity}
                 defaultValue={listing.quantity}
                 onChange={handleSelectChange}>

                  {[...Array(parseInt(25)).keys()].map((number) =>
                      <option value={number}>{number}</option>
                  )}
               </select>
           </div>

           </div>
           &nbsp;
         </div>
        
    </div>
  ));
  return (
    <div>
        {cartItemCount == 1?
          <div>
              <GetOrderSummarySection/>
          <h3>{cartItemCount} item in your cart</h3>         
            {cartCheckout}
          </div>
          :
          <div>
            <GetOrderSummarySection/>

            <h3>{cartItemCount} items in your cart</h3>
            {cartCheckout}
        </div>
        }
    </div>
  );
}