import React, { useState }from 'react';
import Post from '../../helper/posts';

import Header from '../../components/Header'
import Footer from '../../components/Footer'
import Cart from './Cart';
import AuthAlert from './AuthAlert';
import { checkValid } from '../../helper/validation'
import './styles.css';
import Cookies from 'universal-cookie';
import { useNavigate } from "react-router-dom";


import {loadStripe} from '@stripe/stripe-js';
import {
  CardElement,
  Elements,
  useStripe,
  useElements,
} from '@stripe/react-stripe-js';

function BillingForm(props) {
  const stripe = useStripe();
  const elements = useElements();
  console.log(props)
  let billing_address_line1 = React.createRef();
  let billing_address_line2 = React.createRef();
  let billing_address_city = React.createRef();
  let billing_address_country = React.createRef();
  let billing_address_state = React.createRef();
  let billing_address_zip = React.createRef();
  let billing_address_name = React.createRef();

  const [billingAddressLine1Errors, setBillingAddressLine1Errors] = useState('')
  const [billingAddressLine2Errors, setBillingAddressLine2Errors] = useState('')
  const [billingAddressCityErrors, setBillingAddressCityErrors] = useState('')
  const [billingAddressCountryErrors, setBillingAddressCountryErrors] = useState('')
  const [billingAddressStateErrors, setBillingAddressStateErrors] = useState('')
  const [billingAddressZipErrors, setBillingAddressZipErrors] = useState('')
  const [billingAddressNameErrors, setBillingAddressNameErrors] = useState('')


  //https://stripe.com/docs/payments/save-and-reuse#web-collect-card-details

  async function confirmCardSetup() {
    let cookies = new Cookies()
    let user = cookies.get('user')
    // let result = await Post('/submitorder', {user})
    let clientSecret = cookies.get('client_secret')
    
    console.log('sec', clientSecret)
    const result = await stripe.confirmCardSetup(clientSecret.clientSecret, {
      payment_method: {
        card: elements.getElement(CardElement),
        billing_details: {
          name: 'Name Test',
        },
      }
    });

    console.log('result', result)

    if (result.error) {
      console.log('confirmCardsetup error', result.error)
      // Display result.error.message in your UI.
    } else {
      console.log('confirmCardSetup success', result);
      // The setup has succeeded. Display a success message and send
      // result.setupIntent.payment_method to your server to save the
      // card to a Customer
      props.setSelectedCard(result.setupIntent.payment_method)
      props.setShowBillingInfo(false)
      props.setShowConfirmOrder(true)
    }
  };

  return (
    <div>
    {false == true
      ?<div></div>
      :<div>

        {true &&
          <form method="post">
            <label className="label checkoutFields" htmlFor="">
              Street Address
            </label>
            <input
              id="address"
              name="address"
              statehandler="address_line1"
              ref="address_line1"
              type="text"
              placeholder=""
              ref={billing_address_line1}
              onBlur={(e)=>{
                setBillingAddressLine1Errors(checkValid(billing_address_line1.current.value, 'addressInvalid'))}}
              onChange={(e)=>{
                setBillingAddressLine1Errors(checkValid(billing_address_line1.current.value, 'addressInvalid'))}}
              className={`input checkoutFields ${ billingAddressLine1Errors ? `inputErrors`:''}`}
            />
            <p>
              {billingAddressLine1Errors}
            </p>
            <label className="label checkoutFields" htmlFor="">
              Suite/Unit (optional)
            </label>
            <input
              id="suite"
              name="suite"
              ref="address_line2"
              statehandler="address_line2"
              type="text"
              placeholder=""
              ref={billing_address_line2}
              onBlur={(e)=>{
                setBillingAddressLine2Errors(checkValid(billing_address_line2.current.value, 'addressline2Invalid'))}}
              onChange={(e)=>{
                setBillingAddressLine2Errors(checkValid(billing_address_line2.current.value, 'addressline2Invalid'))}}
              className={`input checkoutFields" ${ billingAddressLine2Errors ? `inputErrors`:''}`}
            />
            <p>
              {billingAddressLine2Errors}
            </p>

            <label className="label checkoutFields" htmlFor="">
              City
            </label>

            <input
              id="city"
              name="city"
              ref="address_city"
              statehandler="address_city"
              type="text"
              placeholder=""
              ref={billing_address_city}
              onBlur={(e)=>{
                setBillingAddressCityErrors(checkValid(billing_address_city.current.value, 'cityInvalid'))}}
              onChange={(e)=>{
                setBillingAddressCityErrors(checkValid(billing_address_city.current.value, 'cityInvalid'))}}
              className={`input checkoutFields ${ billingAddressCityErrors ? `inputErrors`:''}`}
            />
            <p>
              {billingAddressCityErrors}
            </p>

            <input
              id="country"
              name="country"
              ref="address_country"
              statehandler="address_country"
              type="text"
              placeholder=""
              ref={billing_address_country}
              onBlur={(e)=>{
                setBillingAddressCountryErrors(checkValid(billing_address_country.current.value, 'countryInvalid'))}}
              onChange={(e)=>{
                setBillingAddressCountryErrors(checkValid(billing_address_country.current.value, 'countryInvalid'))}}
              className={`hidden input checkoutFields ${ billingAddressCountryErrors ? `inputErrors`:''}`}
            />
            <label className="label checkoutFields" htmlFor="">
              State
            </label>

            <input
              id="state"
              name="state"
              ref="address_state"
              statehandler="address_state"
              type="text"
              placeholder=""
              ref={billing_address_state}
              onBlur={(e)=>{
                setBillingAddressStateErrors(checkValid(billing_address_state.current.value, 'stateInvalid'))}}
              onChange={(e)=>{
                setBillingAddressStateErrors(checkValid(billing_address_state.current.value, 'stateInvalid'))}}
              className={`input checkoutFields ${ billingAddressStateErrors ? `inputErrors`:''}`}
              />
            <p>
              {billingAddressStateErrors}
            </p>
            <label className="label checkoutFields" htmlFor="">
              Zip Code
            </label>

            <input
              id="zip"
              name="zip"
              ref="address_zip"
              statehandler="zip"
              type="text"
              placeholder=""
              ref={billing_address_zip}
              className={`input checkoutFields ${ billingAddressZipErrors ? `inputErrors`:''}`}
              onBlur={(e)=>{
                setBillingAddressZipErrors(checkValid(billing_address_zip.current.value, 'zipInvalid'))}}
              onChange={(e)=>{
                setBillingAddressZipErrors(checkValid(billing_address_zip.current.value, 'zipInvalid'))}}
              />
            <p>
              {billingAddressZipErrors}
            </p>


            <label className="label checkoutFields" htmlFor="">
              Name (first & last)
            </label>

            <input
              id="name"
              name="name"
              ref="name"
              statehandler="address_name"
              type="text"
              placeholder=""
              ref={billing_address_name}
              onBlur={(e)=>{
                setBillingAddressNameErrors(checkValid(billing_address_name.current.value, 'nameInvalid'))}}
              onChange={(e)=>{
                setBillingAddressNameErrors(checkValid(billing_address_name.current.value, 'nameInvalid'))}}
              className={`input checkoutFields ${ billingAddressNameErrors ? `inputErrors`:''}`}
            />
            <p>
              {billingAddressNameErrors}
            </p>

            <label className="label">
              Card details
            </label>
            <CardElement
              className="StripeElement"
            />
            <button onClick={(e)=>{e.preventDefault(); props.setShowBillingInfo(false); props.setShowShippingInfo(true)}}>
              Go Back</button>
            <button onClick={(e)=>{
              e.preventDefault()
              if(props.selectedCard != 'intialized' && props.selectedCard != '') {
                confirmCardSetup()
              } else {
                props.setShowBillingInfo(false)
                props.setShowConfirmOrder(true)
              }
              // saveCardInfo(
              //   billing_address_name,
              //   billing_address_city,
              //   billing_address_country,
              //   billing_address_line1,
              //   billing_address_line2,
              //   billing_address_zip,
              //   billing_address_state)
                // props.setShowBillingInfo(false)
                // props.setShowConfirmOrder(true)
                }}>Continue To Confirm Order</button>
          </form>
        }
      </div>
    }
    </div>
  )
}




export default function Checkout() {
  const history = useNavigate()
  const stripePromise = loadStripe('pk_test_Qdj8Q3oSkgyhszzix2MIo026');

  let address_line1 = React.createRef();
  let address_line2 = React.createRef();
  let address_city = React.createRef();
  let address_country = React.createRef();
  let address_state = React.createRef();
  let address_zip = React.createRef();
  let address_name = React.createRef();

  const [addressLine1Errors, setAddressLine1Errors] = useState('')
  const [addressLine2Errors, setAddressLine2Errors] = useState('')
  const [addressCityErrors, setAddressCityErrors] = useState('')
  const [addressCountryErrors, setAddressCountryErrors] = useState('')
  const [addressStateErrors, setAddressStateErrors] = useState('')
  const [addressZipErrors, setAddressZipErrors] = useState('')
  const [addressNameErrors, setAddressNameErrors] = useState('')

  const [currentSelectedAddress, setCurrentSelectedAddress] = useState('')
  const [currentSelectedBilling, setCurrentSelectedBilling] = useState('sameAsShipping')

  function updateBillingSelection(selectedBilling) {
    // setShowShippingAdder(false)
    setCurrentSelectedBilling(selectedBilling)
  }

  function isBillingSelected(selectedBilling) {
    if (currentSelectedBilling == selectedBilling) {
      return true;
    }
    return false;
  }

  function getAddressSelector(customer) {

    if(customer && customer.shipping && currentSelectedAddress != 'prevSaved') {
      setCurrentSelectedAddress('prevSaved');
    }
    let addressSelector = '';
      addressSelector = <div>
        {(customer && customer.shipping) &&
        <div className={`row selectionBoxLength ${
            currentSelectedAddress == 'prevSaved' ? `selectedBoxBorder` : ''}`}
          onClick={() => {
            setCurrentSelectedAddress('prevSaved');
            setShowShippingAdder(false)}}>
          <p>Name: { customer.shipping.name}</p>
          <p>City:{ customer.shipping.address.city}</p>
          <p>Country:{ customer.shipping.address.country}</p>
          <p>line1:{ customer.shipping.address.line1}</p>
          <p>line2:{ customer.shipping.address.line2}</p>
          <p>zip code:{ customer.shipping.address.postal_code}</p>
          <p>state: { customer.shipping.address.state}</p>

          <span />

          <input
            className='serverLengthRadio hidden'
            type="radio"
            checked
            onChange={() => {}}
            name="example[radio_example]"
            value="2"
          />
        </div>
      }
        </div>
    return addressSelector;
  }


  function getShippingAdder() {
    let shippingAdder = '';
    shippingAdder = <div>
    <label className="label" htmlFor="">
      Street Address
    </label>
    <input
      id="address"
      name="address"
      ref="address_line1"
      type="text"
      placeholder=""
      ref={address_line1}
      className='input'
      onBlur={(e)=>{
        setAddressLine1Errors(checkValid(address_line1.current.value, 'addressInvalid'))}}
      onChange={(e)=>{
        setAddressLine1Errors(checkValid(address_line1.current.value, 'addressInvalid'))}}
    />

    <label className="label" htmlFor="">
      Suite/Unit (optional)
    </label>

    <input
      id="suite"
      name="suite"
      ref="address_line2"
      type="text"
      placeholder=""
      ref={address_line2}
      className="input"
      onBlur={(e)=>{
        setAddressLine2Errors(checkValid(address_line2.current.value, 'addressline2Invalid'))}}
      onChange={(e)=>{
        setAddressLine2Errors(checkValid(address_line2.current.value, 'addressline2Invalid'))}}
    />

    <label className="label" htmlFor="">
      City
    </label>

    <input
      id="city"
      name="city"
      ref="address_city"
      type="text"
      placeholder=""
      ref={address_city}
      className="input"
      onBlur={(e)=>{
        setAddressCityErrors(checkValid(address_city.current.value, 'cityInvalid'))}}
      onChange={(e)=>{
        setAddressCityErrors(checkValid(address_city.current.value, 'cityInvalid'))}}
    />

    <input
      id="country"
      name="country"
      ref="address_country"
      type="text"
      placeholder=""
      ref={address_country}
      className="hidden"
      onBlur={(e)=>{
        setAddressCountryErrors(checkValid(address_country.current.value, 'countryInvalid'))}}
      onChange={(e)=>{
        setAddressCountryErrors(checkValid(address_country.current.value, 'countryInvalid'))}}
    />
    <label className="label" htmlFor="">
      State
    </label>

    <input
      id="state"
      name="state"
      ref="address_state"
      type="text"
      placeholder=""
      ref={address_state}
      className="input"
      onBlur={(e)=>{
        setAddressStateErrors(checkValid(address_state.current.value, 'stateInvalid'))}}
      onChange={(e)=>{
        setAddressStateErrors(checkValid(address_state.current.value, 'stateInvalid'))}}
    />
    <label className="label" htmlFor="">
      Zip Code
    </label>

    <input
      id="zip"
      name="zip"
      ref="address_zip"
      type="text"
      placeholder=""
      ref={address_zip}
      className="input"
      onBlur={(e)=>{
        setAddressZipErrors(checkValid(address_zip.current.value, 'zipInvalid'))}}
      onChange={(e)=>{
        setAddressZipErrors(checkValid(address_zip.current.value, 'zipInvalid'))}}
    />


    <label className="label" htmlFor="">
      Name
    </label>

    <input
      id="name"
      name="name"
      ref="name"
      type="text"
      placeholder=""
      ref={address_name}
      className="input"
      onBlur={(e)=>{
        setAddressNameErrors(checkValid(address_name.current.value, 'nameInvalid'))}}
      onChange={(e)=>{
        setAddressNameErrors(checkValid(address_name.current.value, 'nameInvalid'))}}
    />

    </div>
    return shippingAdder;
  }

function getCartSection() {
  let cartSection =
  <div className="row">
    <Cart/>
  </div>
  return cartSection;
}
function handleAddNewAddress(){
  setShowShippingAdder(true)
  setCurrentSelectedAddress('')
}
function handleAddNewCard(){
  return true;
}
function handleLoginBtnClick(){
  let cookies = new Cookies()
  let user = cookies.get('user')
  setShowShippingInfo(true)
  setShowShippingAdder(false)
  if(user && user.customer && !user.customer.shipping) {
    setShowShippingAdder(true)  
  }
  
  setShowBillingInfo(false)
  setShowRegistrationForm(false)
  setShowLoginForm(false)
}


async function handleContinueToBillingInfoClick() {
  console.log('CLCIKED')
  let cookies = new Cookies()
  let user = cookies.get('user')
  let clientSecret = await Post('createIntent', {user})
  console.log('clientS', clientSecret)
  cookies.set('client_secret', {clientSecret: clientSecret.data.data})
  setShowShippingInfo(false)
  setShowBillingInfo(true)
  if(!paymentMethods) {
    setShowCardAdder(true)
    setSelectedCard('')
  }

}

async function completeOrder() {
  console.log(paymentMethod);
  let cookies = new Cookies()
  let cart = cookies.get('cart')
  let user = getUser()
  let address = {}
  if(currentSelectedAddress == 'prevSaved') {
    address = {
      line1: user.customer.shipping.address.line1,
      line2: user.customer.shipping.address.line2,
      country: user.customer.shipping.address.country,
      state: user.customer.shipping.address.state,
      city: user.customer.shipping.address.city,
      postal_code: user.customer.shipping.postal_code,
      name: user.customer.shipping.name
    }
    } else {
      address = {
        line1: address_line1.current.value,
        line2: address_line2.current.value,
        country: address_country.current.value,
        state: address_state.current.value,
        city: address_city.current.value,
        postal_code: address_zip.current.value,
        name: address_name.current.value,
      }
    }

  const {errors, result} = await Post('submitorder', {
    // cart,
    address,
    user,
    paymentMethod: selectedCard,
  });
  if(!errors){
    cookies.set('cart', [])
    history.push('/account');
  } else {
    // display errors
    console.log(errors)
  } 
}

function isLoggedIn() {
  let cookies = new Cookies()
  let loginToken = cookies.get('login_token')
  console.log('islogged', loginToken)
  if(loginToken){
    console.log('ss')
    return true
  } else {
    return false
  } 
}


function updateSelection (selectionId) {
  setSelectedCard(selectionId);
  return selectionId
}
function CardSelector(props){
  console.log('props',props, selectedCard)
  if(props.sources && props.sources.length > 0){
    if(selectedCard == 'initalized') {
      setSelectedCard(props.sources[0].card.id)
    }
    console.log('propssss', props.selectedCard)
  let selectorComponent = props.sources.map(item => (
    <div
      className={`item row selectionBoxLength ${
        selectedCard == item.card.id ? 'selectedBoxBorder' : ''
      } `}
      key={item.card.id}
      onClick={() => {updateSelection(item.card.id)}}>
      <p>{item.card.brand} ending in: {item.card.last4}</p>
      <span />
      <input
        className='serverLengthRadio hidden'
        type="radio"
        checked
        onChange={() => {}}
        name="example[radio_example]"
        value="2"
      />
    </div>
  ));
  return selectorComponent
} else {
  return <div></div>
}
}

function getPaymentMethods() {
  let cookies = new Cookies()
  let paymentMethods = cookies.get('customer_paymentmethods')
  return paymentMethods
}


function getUser() {
  let cookies = new Cookies()
  let user = cookies.get('user')
  console.log(user)
  return user
}

  let user = getUser()
  let paymentMethods = getPaymentMethods();

  const [billingAddress, setBillingAddress] = useState({})
  const [userCart, setUserCart] = useState({})
  const [showBillingInfo, setShowBillingInfo] = useState(false)
  const [showConfirmOrder, setShowConfirmOrder] = useState(false)
  const [showShippingAdder, setShowShippingAdder] = useState(false)
  const [showLoginForm, setShowLoginForm] = useState(true)
  const [showRegistrationForm, setShowRegistrationForm] = useState(true)
  const [showShippingInfo, setShowShippingInfo] = useState(true) 
  const [showCardAdder, setShowCardAdder] = useState(false)
  const [selectedCard, setSelectedCard] = useState('initalized')
  const [paymentMethod, setPaymentMethod] = useState('')

  return (
    <div>
      <Header />
  <div className="root">
    &nbsp;
    <div className="container">
      <div className="six columns">
        <div className="cardInfoContainer">
          <div className={`${(showLoginForm && !isLoggedIn()) ? "" : "hidden"}`}>  
            <div>
              <AuthAlert
                handleLoginBtnClick={handleLoginBtnClick}
              />
              </div>
        </div>

        <div className={(showShippingInfo && isLoggedIn())? "" : "hidden"}>
          <h3>Shipping Info</h3>

          {(user && user.customer.shipping) ?
          <div>
          {getAddressSelector(user.customer)}
          {!showShippingAdder &&
            <div>
              <button onClick={handleAddNewAddress}>Add New Address</button>
            </div>
          }
          {showShippingAdder &&
            <div>
              <div>
                {getShippingAdder()}
              </div>
            </div>
          }</div>:
          <div>

              <div>
                {getShippingAdder()}
              </div>
            </div>
          
          }

          <div>
            <button onClick={()=>{
              handleContinueToBillingInfoClick()}}>
                Continue to billing info</button>
          </div>
        </div>

        <div className={showBillingInfo==true ? "" : "hidden"}>
            <h2>Card Info</h2>
      {!showCardAdder &&
        <div>
          <CardSelector setSelectedCard={setSelectedCard} selectedCard={selectedCard} sources={paymentMethods} />

          <button onClick={()=>{setShowCardAdder(true); setSelectedCard('')}}>Add New Card</button>
      <button onClick={()=>{setShowShippingInfo(true); setShowBillingInfo(false)}}>
        Go Back
      </button>

      <button onClick={()=>{setShowBillingInfo(false); setShowConfirmOrder(true)}}>
        Continue To Confirm Order
      </button>
        </div>
      }
      {showCardAdder &&
        <div>
          <CardSelector setSelectedCard={setSelectedCard} selectedCard={selectedCard} sources={paymentMethods} />
          <Elements stripe={stripePromise}>
            <BillingForm
              setSelectedCard={setSelectedCard}
              setShowShippingInfo={setShowShippingInfo}
              setShowBillingInfo={setShowBillingInfo}
              setShowConfirmOrder={setShowConfirmOrder}
           />
          </Elements>
      {/* <button onClick={()=>{setShowShippingInfo(true);setShowBillingInfo(false)}}>
        Go Back
      </button> */}

      {/* <button onClick={()=>{setShowBillingInfo(false); setShowConfirmOrder(true)}}>
        Continue To Confirm Order
      </button> */}
        </div>
      }

          </div>
      {showConfirmOrder &&
        <div>
          <h2>Confirm Your Order</h2>
          <p>Card:</p>
          <p>shipping info:</p>
          <button onClick={()=>{
            setShowBillingInfo(true);
            setShowConfirmOrder(false)}}>
            Go Back
          </button>

            <button
              className="button"
              onClick={(e)=>{completeOrder(e); window.scrollTo(0, 0);}}>
                Complete Order
            </button>
              <p>By clicking Complete Order, you are agreeing to our
              <a target="_blank" href="/terms">Terms of Service</a> and
              <a target="_blank" href="/privacy">Privacy Policy</a>
            </p>
        </div>
      }

        </div>
      </div>

      <div className="six columns">
        {getCartSection()}
      </div>
    </div>
  </div>

      <Footer />
    </div>
  );
}