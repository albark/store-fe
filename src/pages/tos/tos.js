import Footer from '../../components/Footer'
import './styles.css';


export default function Tos() {
    const company_name = 'A2 Mushrooms LLC';
    const company_name_short = 'A2 Mushrooms';
    const company_address = '709 S 1ST ST, Ann Arbor, MI 48103'
  return (
    <div className="root">
    <div className="container">

    <h1 class="centered">Terms & Conditions</h1>

    <hr class="subHeadingLine" />
    <p>
    {company_name}. (“{company_name_short}”, “we”, “us” or “our”) welcomes you to this site (the “Website”). These terms and conditions of service (collectively, with {company_name_short}’s Privacy Policy and DMCA Copyright Policy, the “Terms of Service” or “Agreement”) govern your use of the Website and the services, features, content or applications operated by {company_name_short} (together with the Website, the “Services”), and provided to the Subscriber (the “Subscriber”, “you” or “your”). Please read these Terms of Service carefully before using the Services. These Terms of Service apply to all users of the Services, including without limitation any sub-users. Using the Services in any manner constitutes your acceptance and agreement to be bound by these Terms of Service, and all other operating rules, policies and procedures that may be published from time to time on the Website by us, each of which is incorporated by reference and each of which may be updated from time to time without notice to you. IF YOU DO NOT AGREE TO THESE TERMS OF SERVICE, YOU MAY NOT USE THE SERVICES AND YOU SHOULD EXIT THE WEBSITE AND STOP USE OF THE SERVICES IMMEDIATELY.
    </p>
    <p>
    {company_name_short} reserves the right, at any time and from time to time, to amend or to modify these Terms of Service without prior notice to you, provided that if any such alterations constitute a material change to these Terms of Service, {company_name_short} will notify you by posting an announcement on the Website. Amendments and modifications shall take effect immediately when posted on the Website. By continuing to access or use the Services after any such amendments or modifications, you agree to be bound by such amended or modified Terms of Service. For this reason, we encourage you to review the Terms of Service whenever you use the Services. If you do not agree to any change to these Terms of Services, then you must immediately stop using the Services.
    </p>

    <h2 class="subHeading">1. Eligibility & Registration</h2>
    <hr class="subHeadingLine" />

    <p>
    1.1 The Services are not targeted towards, nor intended for use by, anyone under the age of 13. By using the Services, you represent and warrant that you are 13 years of age or older. If you are under the age of 13, you may not, under any circumstances or for any reason, use the Services. We may, in our sole discretion, refuse to offer the Services to any person or entity and change its eligibility criteria at any time. You are solely responsible for ensuring that these Terms of Service are in compliance with all laws, rules and regulations applicable to you and the right to access the Services is revoked where these Terms of Service or use of the Services is prohibited or to the extent offering, sale or provision of the Services conflicts with any applicable law, rule or regulation. Further, the Services are offered only for your use, and not for the use or benefit of any third party.
    </p>
    <p>
    1.2 To sign up for the Services, you must register for an account on the Services (an “Account”). You must provide accurate and complete information and keep your Account information updated. You shall not: (i) select or use as a username a name of another person with the intent to impersonate that person; (ii) use as a username a name subject to any rights of a person other than you without appropriate authorization; or (iii) use, as a username, a name that is otherwise offensive, vulgar or obscene. You are solely responsible for the activity that occurs on your Account, regardless of whether the activities are undertaken by you, your employees or a third party (including your contractors or agents), and for keeping your Account password secure. You may never use another person’s user account or registration information for the Services without permission. You must notify us immediately of any change in your eligibility to use the Services (including any changes to or revocation of any licenses from state authorities), breach of security or unauthorized use of your Account. You should never publish, distribute or post login information for your Account. You shall have the ability to delete your Account, either directly or through a request made to one of our employees or affiliates. {company_name_short} will not be liable for any loss or damage as a result of your failure to provide us with accurate information or to keep your Account secure.
    </p>

    <h2 class="subHeading">2. Content</h2>
    <hr class="subHeadingLine" />
    <p>
    2.1 For purposes of these Terms of Service, the term “Content” includes, without limitation, information, data, text, written posts and comments, software, scripts, graphics, and interactive features generated, provided, or otherwise made accessible on or through the Services. For the purposes of this Agreement, “Content” also includes all User Content (as defined below).
    </p>
    <p>
    2.2 All Content added, created, uploaded, submitted, distributed, or posted to the Services by users (collectively “User Content”), whether publicly posted or privately transmitted, is the sole responsibility of the person who originated such User Content. You represent that all User Content provided by you is accurate, complete, up-to-date, and in compliance with all applicable laws, rules and regulations. You acknowledge that all Content, including User Content, accessed by you using the Services is at your own risk and you will be solely responsible for any damage or loss to you or any other party resulting therefrom. We do not guarantee that any Content you access on or through the Services is or will continue to be accurate.
    </p>
    <p>
    2.3 The Services may contain Content specifically provided by us, our partners or our users and such Content is protected by copyrights, trademarks, service marks, patents, trade secrets or other proprietary rights and laws. You shall abide by and maintain all copyright notices, information, and restrictions contained in any Content accessed through the Services.
    </p>
    <p>
    2.4 Subject to these Terms of Service, we grant each user of the Services a worldwide, non-exclusive, non-sublicensable and non-transferable license to use (i.e., to download and display locally) Content solely for purposes of using the Services. Use, reproduction, modification, distribution or storage of any Content for other than purposes of using the Services is expressly prohibited without prior written permission from us. You shall not sell, license, rent, or otherwise use or exploit any Content for commercial use or in any way that violates any third party right.
    </p>
    <p>
    2.5 By submitting any User Content to the Website, excluding privately transmitted User Content, you hereby do and shall grant us a worldwide, non-exclusive, perpetual, royalty-free, fully paid, sublicensable and transferable license to use, aggregate, reproduce, distribute, prepare derivative works of, display, perform, and otherwise fully exploit such User Content in connection with the Website, the Services and our (and our successors’ and assigns’) businesses, including without limitation for promoting and redistributing part or all of the Website or the Services (and derivative works thereof) in any media formats and through any media channels (including, without limitation, third party websites and feeds), and including after your termination of your Account or the Services. You also hereby do and shall grant each user of the Website and/or the Services a non-exclusive, perpetual license to access any of your User Content that is available to such user on the Website, and to use, reproduce, distribute, prepare derivative works of, display and perform such User Content, including after your termination of your Account or the Services. By submitting any User Content to the Services other than on the Website, you hereby do and shall grant us a worldwide, non-exclusive, perpetual, royalty-free, fully paid, sublicensable and transferable license to use, aggregate, reproduce, distribute, prepare derivative works of, display, and perform such User Content solely for the purpose of providing the Services. For clarity, the foregoing licenses granted to us and our users does not affect your other ownership or license rights in your User Content, including the right to grant additional licenses to your User Content, unless otherwise agreed in writing. You represent and warrant that you have all rights to grant such licenses to us without infringement or violation of any third party rights, including without limitation, any privacy rights, publicity rights, copyrights, trademarks, contract rights, or any other intellectual property or proprietary rights.
    </p>
    <p>
    2.6 Some Content will be marked on the Service as “Creative Commons Content”. Creative Commons Content will be identified with a Creative Commons icon. We hereby grant each user of the Services a license to Creative Commons Content under the Creative Commons CC BY-NC-SA 4.0 US license, available at hthe "Creative Commons License. You agree to abide by the terms of the Creative Commons License when using Creative Commons Content.
    </p>

    <h2 class="subHeading">3. Rules of Conduct</h2>
    <hr class="subHeadingLine" />
    <p>
    3.1 As a condition of use, you promise not to use the Services for any purpose that is prohibited by these Terms of Service. You are responsible for all of your activity in connection with the Services.
    </p>
    <p>
    3.2 You agree that you will not transmit, distribute, post, store, link, or otherwise traffic in Content, information, software, or materials on or through the Service that (i) is unlawful, threatening, abusive, harassing, defamatory, libelous, deceptive, fraudulent, invasive of another's privacy, tortious, offensive, profane, contains or depicts pornography that is unlawful, or is otherwise inappropriate as determined by us in our sole discretion, (ii) you know is false, misleading, untruthful or inaccurate, (iii) constitutes unauthorized or unsolicited advertising, (iv) impersonates any person or entity, including any of our employees or representatives, or (v) includes anyone’s identification documents or sensitive financial information. {company_name_short} may permit, in its sole discretion, adult websites that abide by state and federal law and regulation.
    </p>
    <p>
    3.3 You shall not: (i) take any action that imposes or may impose (as determined by us in our sole discretion) an unreasonable or disproportionately large load on our (or our third party providers’) infrastructure; (ii) interfere or attempt to interfere with the proper working of the Services or any activities conducted on the Services; (iii) bypass, circumvent or attempt to bypass or circumvent any measures we may use to prevent or restrict access to the Services (or other accounts, computer systems or networks connected to the Services); (iv) run any form of auto-responder or “spam” on the Services; (v) use manual or automated software, devices, or other processes to “crawl” or “spider” any page of the Website; (vi) harvest or scrape any Content from the Services; (vii) use the Services for high risk activities including but not limited to the operation of nuclear facilities, air traffic control, life support systems, or any other use where the failure of service could lead to death, personal injury, or environmental damage; or (viii) otherwise take any action in violation of our guidelines and policies.
    </p>
    <p>
    3.4 You shall not (directly or indirectly): (i) decipher, decompile, disassemble, reverse engineer or otherwise attempt to derive any source code or underlying ideas or algorithms of any part of the Services (including without limitation any application), except to the limited extent applicable laws specifically prohibit such restriction, (ii) modify, translate, or otherwise create derivative works of any part of the Services, or (iii) copy, rent, lease, distribute, or otherwise transfer any of the rights that you receive hereunder. You shall abide by all applicable local, state, national and international laws and regulations.
    </p>
    <p>
    3.5 We also reserve the right to access, read, preserve, and disclose any information as we reasonably believe is necessary to (i) satisfy any applicable law, regulation, legal process or governmental request, (ii) enforce these Terms of Service, including investigation of potential violations hereof, (iii) detect, prevent, or otherwise address fraud, security or technical issues, (iv) respond to user support requests, or (v) protect the rights, property or safety of us, our users and the public.
    </p>
    <p>
    3.6 Subscribers are restricted from registering multiple Accounts with the same billing details without first notifying {company_name_short} of that intent. Otherwise, {company_name_short} shall have the right to automatically flag such Accounts as fraudulent or abusive, and {company_name_short} may, without notification to the Subscriber of such Account, suspend the service of such Account or any other Account used by such Subscriber. The use of referral codes by multiple Accounts having the same billing profile is not allowed. {company_name_short} also reserves the right to terminate a Subscriber's Account if it is targeted by malicious activity from other parties.
    </p>
    <p>
    3.7 As a reward for being early adopters of the Services, Subscribers with grandfathered Accounts shall receive free bandwidth for the duration that such Account is operative and conducts its operations in compliance with these Terms of Service (“Grandfathered Accounts”). The free bandwidth may only be used directly by the Subscriber of such Grandfathered Account. Notwithstanding the foregoing, Subscribers of Grandfathered Accounts must NOT: (i) run Torrents for download or Seed Servers, TOR, or services that include content of an adult or pornographic nature; (ii) resell services through their Account to provide free bandwidth to other individuals; or (iii) transfer the Account ownership to another individual or entity, or otherwise circumvent the intended fair usage of free bandwidth by distributing it freely to others. Failure of Subscribers of Grandfathered Accounts to follow these terms will result in the revocation of their Accounts’ grandfathered status.
    </p>
    <p>
    3.8 The enumeration of violations in this Section 3 of these Terms of Service is not meant to be exclusive, and {company_name_short} provides notice hereby that it has and will exercise its authority to take whatever action is necessary to protect the Services, Subscribers, and third parties from acts that would be inimical to the purposes of this Section 3 of these Terms of Service.
    </p>

    <h4>Lawful Use of the Network</h4>
    <p>
    3.9 In using the Services, Subscribers must comply with, and refrain from violations of, any right of any other person, entity, or law, or any contractual duty, including but not limited to the United States Code, the Code of Federal Regulations, and the Michigan Revised Statutes, including but not limited to those statutes forbidding: (a) distribution of child pornography, (b) forgery, identity theft, misdirection or interference with electronic communications, (c) invasion of privacy, (d) violations of the CANSPAM Act, (e) collection of excessive user data from children, or other improper data collection activities, (f) securities violations, wire fraud, money laundering, or terrorist activities, or (f) false advertising, propagating or profiting from frauds and unfair schemes. Subscribers will also comply with the affirmative requirements of law governing use of the Services, including but not limited to: (i) disclosure requirements, including those regarding notification of security breaches, (ii) records maintenance for regulated industries, and (iii) financial institution safeguards.
    </p>

    <h4>Agreed Use of Allotted Network Resources</h4>
    <p>
    3.10 Subscribers shall not use any method to circumvent the provisions of these Terms of Service, or to obtain Services in excess of those for which they contract with {company_name_short}. Subscribers shall use only those IP addresses that are assigned to them by {company_name_short}, and shall not use any IP addresses outside of their assigned range. Subscribers shall not use any mechanism to exceed the amount of resources assigned to them through the Services, or to conceal such activities.
    Injurious Code
    </p>
    <p>
    3.11 Subscribers may not use the Services to distribute, receive communications or data gleaned from, or execute any action directed by any type of injurious code, including but not limited to: (i) trojans, (ii) key loggers, (iii) viruses, (iv) malware, (v) botnets, (vi) denial of service attacks, (vii) flood or mail bombs, (viii) logic bombs, or (ix) other actions which {company_name_short} reserves the sole right to determine to be malicious in intent.
    </p>
    <h4>Email Violations</h4>
    <p>
    3.12 In addition to being forbidden from performing any acts made illegal by the CAN-SPAM Act, Subscribers may not send bulk email utilizing their resources on the Services unless they maintain a double-authorized list of subscribed members including IP addresses and relevant contact information, along with following guidelines for including removal links with all sent emails according to the CAN-SPAM Act. Subscribers are forbidden from taking any action that would result in their IP addresses, or any IP address associated with {company_name_short} or other Subscribers, being placed on the Spamhaus.org blacklist. {company_name_short} reserves the sole and absolute right to determine whether an email violation has occurred.
    </p>

    <h4>Invasion of Privacy, Defamation, or Harassment</h4>
    <p>
    3.13 Subscribers may not use the Services in a manner that would violate the lawful privacy rights of any person, or to publish or republish defamatory or libelous statements, or to harass or embarrass, which shall be determined in {company_name_short}’s sole and absolute discretion.
    </p>

    <h4>Violation of Copyright, Trademark, Patent or Trade Secret</h4>
    <p>
    3.14 Subscribers may not use the Services in violation of the copyrights, trademarks, patents or trade secrets of third parties, nor shall they utilize the Services to publish such materials in a manner that would expose them to public view in violation of the law. The provisions of the Digital Millennium Copyright Act of 1998 (“DMCA”) (as required under 17 U.S.C. §512) will apply to issues presented by allegations of copyright violations by third parties. {company_name_short} will, in appropriate circumstances, terminate the accounts of repeat violators. If a third party believes that a Subscriber of {company_name_short} is violating its intellectual property rights, it should notify us by contacting support. A notification should include information reasonably sufficient to permit {company_name_short} to locate the allegedly infringing material, such as the IP address or URL of the specific online location where the alleged infringement is occurring. Please see our DMCA Copyright Policy.
    </p>

    <h4>Export</h4>
    <p>
    3.15 Subscriber shall comply with all applicable export and import control laws and regulations in its use of the Services, and, in particular, Subscriber shall not utilize the Services to export or re-export data or software without all required United States and foreign government licenses. Subscriber assumes full legal responsibility for any access and use of the Services from outside the United States, with full understanding that the same may constitute export of technology and technical data that may implicate export regulations and/or require export license. Should such a license be required, it shall be Subscriber's responsibility to obtain the same, at Subscriber’s sole cost and expense, and in the event of any breach of this duty resulting in legal claims against {company_name_short}, Subscriber shall defend, indemnify and hold {company_name_short} harmless from all claims and damages arising therefrom.
    </p>

    <h4>Acts of Sub-Users</h4>
    <p>
    3.16 Subscribers are responsible for the acts of others utilizing their access to the Services, and will be held responsible for violations of the Services by their sub-users or persons who gain access to the Services using the Subscriber's access codes. Any activity that a Subscriber is prohibited from performing by these Terms of Services is equally prohibited to anyone using the access to the Services of the Subscriber. Accordingly, Subscribers agree to take the following actions outlined in 3.17, 3.18 and 3.19 below to control the activities of those who connect to the Services by any means.
    </p>

    <h4>Access Code Protection</h4>
    <p>
    3.17 Subscribers shall utilize proper security protocols, such as setting strong passwords and access control mechanisms, safeguarding access to all logins and passwords, and verifying the trustworthiness of persons who are entrusted with account access information.
    </p>

    <h4>Notification Regarding these Terms of Service</h4>
    <p>
    3.18 Subscribers shall notify all persons who receive access to the Services of the provisions of these Terms of Service, and shall inform them that the terms of these Terms of Service are binding upon them.
    </p>

    <h4>Remedial Action</h4>
    <p>
    3.19 Subscribers shall notify {company_name_short} if and when they learn of any security breaches regarding the Services, and shall aid in any investigation or legal action that is taken by authorities and/or {company_name_short} to cure the security breach.
    </p>


    <h2 class="subHeading">4. Third Party Services</h2>
    <hr class="subHeadingLine" />
    <p>
    4.1 The Services may permit you to link to other websites, services or resources on the Internet, and other websites, services or resources may contain links to the Services. When you access third party resources on the Internet, you do so at your own risk. These other resources are not under our control, and you acknowledge that we are not responsible or liable for the content, functions, accuracy, legality, appropriateness or any other aspect of such websites or resources. The inclusion of any such link does not imply our endorsement or any association between us and their operators. You further acknowledge and agree that we shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with the use of or reliance on any such content, goods or services available on or through any such website or resource.
    </p>


    <h2 class="subHeading">5. Payments and Billing</h2>
    <hr class="subHeadingLine" />

    <h4>Acceptable Payment Methods</h4>
    <p>
    5.1 {company_name_short} accepts major credit cards. Virtual credit cards and gift cards typically will not be accepted. Other forms of payment may be arranged by contacting {company_name_short} support. Please note that any payment terms presented to you in the process of using or signing up for paid Services are deemed part of this Agreement.
    </p>
    <p>
    5.2 We use third-party payment processors (the “Payment Processors”) to bill you through a payment account linked to your Account on the Services (your “Billing Account”) for use of the paid Services. The processing of payments will be subject to the terms, conditions and privacy policies of the Payment Processors in addition to this Agreement. We are not responsible for error by the Payment Processors. By choosing to use paid Services, you agree to pay us, through the Payment Processors, all charges at the prices then in effect for any use of such paid Services in accordance with the applicable payment terms and you authorize us, through the Payment Processors, to charge your chosen payment provider (your “Payment Method”). You agree to make payment using that selected Payment Method. We reserve the right to correct any errors or mistakes that it makes even if it has already requested or received payment
    </p>

    <h4>Billing and Term</h4>
    <p>
    5.3 The term of this Agreement shall be monthly, to commence on the date that the Subscriber signs up electronically for the Services by creating an Account with an email address. All invoices are denominated, and Subscriber must pay, in U.S. Dollars. Subscribers are typically billed monthly with payments due no later than ten (2) days past the invoice date. On rare occasions, a Subscriber may be billed an amount up to the Subscriber's current balance in an effort to verify the authenticity of the Subscriber's account information. This process ensures that Subscribers without a payment history are not subjected to additional scrutiny. Subscribers are entirely responsible for the payment of all taxes.
    </p>
    <p>
    5.4 Some of the paid Services may consist of an initial period, for which there is a one-time charge, followed by recurring period charges as agreed to by you. By choosing a recurring payment plan, you acknowledge that such Services have an initial and recurring payment feature and you accept responsibility for all recurring charges prior to cancellation. WE MAY SUBMIT PERIODIC CHARGES (E.G., MONTHLY) WITHOUT FURTHER AUTHORIZATION FROM YOU, UNTIL YOU PROVIDE PRIOR NOTICE (RECEIPT OF WHICH IS CONFIRMED BY US) THAT YOU HAVE TERMINATED THIS AUTHORIZATION OR WISH TO CHANGE YOUR PAYMENT METHOD. SUCH NOTICE WILL NOT AFFECT CHARGES SUBMITTED BEFORE WE REASONABLY COULD ACT. TO TERMINATE YOUR AUTHORIZATION OR CHANGE YOUR PAYMENT METHOD, GO TO YOUR BILLING SECTION INSIDE YOUR ACCOUNT.
    </p>
    <p>
    5.5 YOU MUST PROVIDE CURRENT, COMPLETE AND ACCURATE INFORMATION FOR YOUR BILLING ACCOUNT. YOU MUST PROMPTLY UPDATE ALL INFORMATION TO KEEP YOUR BILLING ACCOUNT CURRENT, COMPLETE AND ACCURATE (SUCH AS A CHANGE IN BILLING ADDRESS, CREDIT CARD NUMBER, OR CREDIT CARD EXPIRATION DATE), AND YOU MUST PROMPTLY NOTIFY US OR OUR PAYMENT PROCESSORS IF YOUR PAYMENT METHOD IS CANCELED (E.G., FOR LOSS OR THEFT) OR IF YOU BECOME AWARE OF A POTENTIAL BREACH OF SECURITY, SUCH AS THE UNAUTHORIZED DISCLOSURE OR USE OF YOUR USER NAME OR PASSWORD. IF YOU FAIL TO PROVIDE ANY OF THE FOREGOING INFORMATION, YOU AGREE THAT WE MAY CONTINUE CHARGING YOU FOR ANY USE OF PAID SERVICES UNDER YOUR BILLING ACCOUNT UNLESS YOU HAVE TERMINATED YOUR PAID SERVICES AS SET FORTH ABOVE.
    </p>

    <h4>Arrearages</h4>
    <p>
    5.6 Payments not made within ten (2) days of invoicing will be deemed in arrears. For accounts in arrears, if any amount is more than ten (10) days overdue, without the requirement of providing notice of such arrears, {company_name_short} may suspend service to such account and bring legal action to collect the full amount due, including any attorneys’ fees and costs.
    </p>

    <h4>Suspension for Nonpayment</h4>
    <p>
    5.7 If a Subscriber is past due on their balance, {company_name_short} may send up to three (3) email notifications within a fifteen (15) day period before suspending the Subscriber's account. Servers will be temporarily powered off during the suspension period. {company_name_short} reserves the right to delete the Subscriber's suspended machines after the final termination notice.
    </p>

    <h4>Promotional Credit</h4>
    <p>
    5.8 Only one promotional code is permitted per customer, and may be redeemed only by “new users,” defined as users who are within 30 days of launching their first Droplet.
    </p>

    <h4>Referral Credit</h4>
    <p>
    5.9 Earned credit from making a referral will not expire. All referral payouts will be paid in {company_name_short} credit.
    </p>


    <h2 class="subHeading">6. Warranty Disclaimer</h2>
    <hr class="subHeadingLine" />

    <p>6.1 We have no special relationship with or fiduciary duty to you. You acknowledge that we have no duty to take any action regarding:
     </p>
    <p>
      i. which Subscribers gain access to the Services;
    </p>
    <p>
      ii. what Content you access via the Services; or
    </p>
    <p>
      iii. how you may interpret or use the Content.
    </p>
    <p>
    6.2 You release us from all liability for you having acquired or not acquired Content through the Services. We make no representations concerning any Content contained in or accessed through the Services, and we will not be responsible or liable for the accuracy, copyright compliance, or legality of material or Content contained in or accessed through the Services.
    </p>
    <p>
    6.3 THE SERVICES AND CONTENT ARE PROVIDED “AS IS”, “AS AVAILABLE” AND WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF TITLE, NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, AND ANY WARRANTIES IMPLIED BY ANY COURSE OF PERFORMANCE OR USAGE OF TRADE, ALL OF WHICH ARE EXPRESSLY DISCLAIMED. WE, AND OUR DIRECTORS, EMPLOYEES, AGENTS, SUPPLIERS, PARTNERS AND CONTENT PROVIDERS DO NOT WARRANT THAT: (I) THE SERVICES WILL BE SECURE OR AVAILABLE AT ANY PARTICULAR TIME OR LOCATION; (II) ANY DEFECTS OR ERRORS WILL BE CORRECTED; (III) ANY CONTENT OR SOFTWARE AVAILABLE AT OR THROUGH THE SERVICES IS FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS; OR (IV) THE RESULTS OF USING THE SERVICES WILL MEET YOUR REQUIREMENTS. YOUR USE OF THE SERVICES IS SOLELY AT YOUR OWN RISK.
    </p>

    <h2 class="subHeading">7. Limitation of Liability</h2>
    <hr class="subHeadingLine" />

    <p>
    7.1 IN NO EVENT SHALL WE, NOR OUR DIRECTORS, EMPLOYEES, AGENTS, PARTNERS, SUPPLIERS OR CONTENT PROVIDERS, BE LIABLE UNDER CONTRACT, TORT, STRICT LIABILITY, NEGLIGENCE OR ANY OTHER LEGAL OR EQUITABLE THEORY WITH RESPECT TO THE SERVICES (I) FOR ANY LOST PROFITS, DATA LOSS, COST OF PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES, OR SPECIAL, INDIRECT, INCIDENTAL, PUNITIVE, COMPENSATORY OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER, SUBSTITUTE GOODS OR SERVICES (HOWEVER ARISING), (II) FOR ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE (REGARDLESS OF THE SOURCE OF ORIGINATION), OR (III) FOR ANY DIRECT DAMAGES IN EXCESS OF (IN THE AGGREGATE) OF FEES PAID TO US FOR THE PARTICULAR SERVICES DURING THE IMMEDIATELY PREVIOUS ONE MONTH PERIOD, EVEN IF A2 MUSHROOMS HAD BEEN ADVISED OF, KNEW, OR SHOULD HAVE KNOWN, OF THE POSSIBILITY THEREOF. SUBSCRIBER ACKNOWLEDGES THAT THE FEES PAID BY HIM OR HER REFLECT THE ALLOCATION OF RISK SET FORTH IN THIS AGREEMENT AND THAT A2 MUSHROOMS WOULD NOT ENTER INTO THIS AGREEMENT WITHOUT THESE LIMITATIONS. SUBSCRIBER HEREBY WAIVES ANY AND ALL CLAIMS AGAINST A2 MUSHROOMS ARISING OUT OF SUBSCRIBER'S PURCHASE OR USE OF THE SERVICES, OR ANY CONDUCT OF A2 MUSHROOMS'S DIRECTORS, OFFICERS, EMPLOYEES, AGENTS OR REPRESENTATIVES. YOUR SOLE AND EXCLUSIVE RIGHT AND REMEDY IN CASE OF DISSATISFACTION WITH THE SERVICES OR ANY OTHER GRIEVANCE SHALL BE YOUR TERMINATION AND DISCONTINUATION OF ACCESS TO OR USE OF THE SERVICES.
    </p>

    <h2 class="subHeading">8. Confidentiality</h2>
    <hr class="subHeadingLine" />


    <p>
    8.1 Subscriber shall keep confidential any confidential information to which it is given access, and shall cooperate with {company_name_short}'s efforts to maintain the confidentiality thereof. Subscriber shall not publish to third parties or distribute information or documentation that {company_name_short} provides for purposes of operating and maintaining its systems, including material contained in estimates, invoices, work orders, or other such materials.
    </p>


    <h2 class="subHeading">9. Backup</h2>
    <hr class="subHeadingLine" />
    <p>
    9.1 Subscriber is solely responsible for the preservation of Subscriber's data which Subscriber saves onto its virtual server (the “Data”). Even with respect to Data as to which Subscriber contracts for backup services provided by {company_name_short}, {company_name_short} shall have no responsibility to preserve Data. {company_name_short} shall have no liability for any Data that may be lost, or unrecoverable, by reason of Subscriber’s failure to backup its Data.
    </p>

    <h2 class="subHeading">10. Publicity</h2>
    <hr class="subHeadingLine" />
    <p>
    10.1 Each Subscriber is permitted to state publicly that such Subscriber is a Subscriber of the Services. Each Subscriber agrees that {company_name_short} may include such Subscriber’s name and trademarks in a list of {company_name_short} Subscriber, online or in promotional materials. Each Subscriber also agrees that {company_name_short} may verbally reference such Subscriber as a Subscriber of the Services. Subscriber may opt out of the provisions in this Section 10.1 by contacting support.
    </p>

    <h2 class="subHeading">11. Indemnification</h2>
    <hr class="subHeadingLine" />

    <p>
    11.1 You shall defend, indemnify, and hold harmless us, our affiliates, parents, subsidiaries, any related companies, licensors and partners, and each of our and their respective employees, officers, directors, agents, contractors, directors, suppliers and representatives from all liabilities, claims, and expenses, including reasonable attorneys’ fees, that arise from or relate to your (or any third party using your Account or identity in the Services) use or misuse of, or access to, the Services, Content, or otherwise from your User Content, violation of these Terms of Service or of any law, or infringement of any intellectual property or other right of any person or entity. We reserve the right to assume the exclusive defense and control of any matter otherwise subject to indemnification by you, in which event you will assist and cooperate with us in asserting any available defenses.
    </p>

    <h2 class="subHeading">12. Termination and Access</h2>
    <hr class="subHeadingLine" />
    <p>
    12.1 {company_name_short} reserves the right, in our sole discretion, to terminate your access to all or any part of the Services at any time, with or without notice, effective immediately, including but not limited to as a result of your violation of any of these Terms of Service or any law, or if you misuse system resources, such as, by employing programs that consume excessive network capacity, CPU cycles, or disk IO. Any such termination may result in the forfeiture and destruction of information associated with your Account. {company_name_short} may provide prior notice of the intent to terminate Services to you if such notice will not, in {company_name_short}'s discretion, run counter to the intents and purposes of these Terms of Service. Any fees paid hereunder are non-refundable and any fees owed to {company_name_short} before such termination shall be immediately due and payable, including any liabilities that may have been incurred prior to termination such as {company_name_short}'s costs for collection (including attorneys’ fees) of any such charges or other liabilities. Upon termination, any and all rights granted to Subscriber by this Agreement will immediately be terminated, and Subscriber shall promptly discontinue all use of the Services. If you wish to terminate your Account, you may do so by following the instructions on the Website or through the Services. All provisions of these Terms of Service which by their nature should survive termination shall survive termination, including, without limitation, licenses of User Content, ownership provisions, warranty disclaimers, indemnity and limitations of liability.
    </p>

    <h2 class="subHeading">13. Governing Law</h2>
    <hr class="subHeadingLine" />
    <p>
    13.1 This Agreement shall be governed, construed, and enforced in accordance with the laws of the State of Michigan, without regard to its conflict of laws rules.
    </p>

    <h2 class="subHeading">14. Dispute Resolution</h2>
    <hr class="subHeadingLine" />
    <p>
    14.1 Mindful of the high cost of litigation, you and {company_name_short} agree to the following dispute resolution procedure: in the event of any controversy, claim, action or dispute arising out of or related to: (i) the Website; (ii) this Agreement; (iii) the Services; (iv) the breach, enforcement, interpretation, or validity of this Agreement; or (v) any other dispute between you and {company_name_short} (“Dispute”), the party asserting the Dispute shall first try in good faith to settle such Dispute by providing written notice to the other party (by first class or registered mail) describing the facts and circumstances (including any relevant documentation) of the Dispute and allowing the receiving party 30 days in which to respond to or settle the Dispute. Notice shall be sent (1) if to {company_name} at: {company_address} or (2) if to you at: your last-used billing address or the billing and/or shipping address in your Account information. Both you and {company_name_short} agree that this dispute resolution procedure is a condition precedent that must be satisfied prior to initiating any arbitration or filing any claim against the other party.
    </p>

    <h2 class="subHeading">15. Mandatory Arbitration Agreement and Class Action Waiver</h2>
    <hr class="subHeadingLine" />
    <p>
    15.1 Both you and {company_name_short} agree that any dispute or claim, including without limitation, statutory, contract or tort claims, relating to or arising out of this Agreement or the alleged breach of this Agreement, shall, upon timely written request of either party, be submitted to binding arbitration. The party asserting the claim may elect to have the arbitration be in-person, telephonic or decided based only on written submissions. The arbitration shall be conducted in the city in which the Subscriber is billed. The arbitration shall proceed in accordance with the commercial arbitration rules of the American Arbitration Association (”AAA”) in effect at the time the claim or dispute arose. The arbitration shall be conducted by one arbitrator from AAA or a comparable arbitration service who is selected pursuant to the applicable rules of the AAA. The arbitrator shall issue a reasoned award with findings of fact and conclusions of law, and judgment on the award rendered by the arbitrator may be entered in any court having jurisdiction thereof. Either you or {company_name_short} may bring an action in any court of competent jurisdiction to compel arbitration under this Agreement, or to enforce or vacate an arbitration award. {company_name_short} will pay the fee for the arbitrator and your filing fee, to the extent that it is more than a court filing fee. {company_name_short} agrees that it will not seek reimbursement of its fees and expenses if the arbitrator rules in its favor. You and {company_name_short} waive any right to a trial by jury, so that disputes will be resolved through arbitration. No claim subject to this provision may be brought as a class or collective action, nor may you assert such a claim as a member of a class or collective action that is brought by another claimant. Furthermore, the arbitrator may not consolidate more than one person’s claims, and may not otherwise preside over any form of a representative or class proceeding. Except as may be required by law, neither a party nor an arbitrator may disclose the existence, content, or results of any arbitration hereunder without the prior written consent of both parties.
    </p>


    <h2 class="subHeading">16. Miscellaneous Provisions</h2>
    <hr class="subHeadingLine" />
    <p>

    16.1 Neither you nor {company_name_short} shall be liable for nonperformance of the terms herein to the extent that either you or {company_name_short} are prevented from performing as a result of any act or event which occurs and is beyond your or {company_name_short}’s reasonable control, including, without limitation, acts of God, war, unrest or riot, strikes, any action of a governmental entity, weather, quarantine, fire, flood, earthquake, explosion, utility or telecommunications outages, Internet disturbance, or any unforeseen change in circumstances, or any other causes beyond either party’s reasonable control. The party experiencing the force majeure shall provide the other party with prompt written notice thereof and shall use reasonable efforts to remedy effects of such force majeure.
    </p>
    <p class="extra-bottom-push">
    16.2 This Agreement, including all related agreements and policies incorporated by reference herein, constitutes the entire agreement between the parties related to the subject matter hereof and supersedes any prior or contemporaneous agreement between the parties relating to the Services. A valid waiver hereunder shall not be interpreted to be a waiver of that obligation in the future or any other obligation under this Agreement. The failure of either party to exercise in any respect any right provided for herein shall not be deemed a waiver of any further rights hereunder. In order for any waiver of compliance with these Terms of Service to be binding, we must provide you with written notice of such waiver through one of our authorized representatives. If any provision of this Agreement is prohibited by law or held to be unenforceable, that provision will be severed and the remaining provisions hereof shall not be affected such that this Agreement shall continue in full force and effect as if such unenforceable provision had never constituted a part hereof. This Agreement may be executed in counterparts, each of which shall be deemed an original, but all of which together shall constitute the same instrument. This Agreement may be signed electronically. These Terms of Service are personal to you, and are not assignable, transferable or sublicensable by you except with our prior written consent. We may assign, transfer or delegate any of our rights and obligations hereunder without consent. No agency, partnership, joint venture, or employment relationship is created as a result of these Terms of Service and neither party has any authority of any kind to bind the other in any respect. The section and paragraph headings in these Terms of Service are for convenience only and shall not affect their interpretation. Unless otherwise specified in these Terms of Service, all notices under these Terms of Service will be in writing and will be deemed to have been duly given when received, if personally delivered or sent by certified or registered mail, return receipt requested; when receipt is electronically confirmed, if transmitted by facsimile or e-mail; or the day after it is sent, if sent for next day delivery by recognized overnight delivery service. Electronic notices should be sent to the support department.
    </p>


</div>
<Footer />
</div>
  );
}