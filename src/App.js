import './App.css';
import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";
import Home from "./pages/home/home";
import About from "./pages/about/about";
import Account from "./pages/account/account";
import Blog from "./pages/blog/blog";
import Cart from "./pages/cart/cart";
import Checkout from "./pages/checkout/checkout";
import Contact from "./pages/contact/contact";
import Register from "./pages/register/register";
import Login from "./pages/login/login";
import Logout from "./pages/logout/logout";
import Marketplace from "./pages/marketplace/marketplace";
import Listing from "./pages/listing/listing";
import Privacy from "./pages/privacy/privacy";
import Tos from "./pages/tos/tos";
import FourOhFour from "./pages/fourOhFour/fourOhFour";

function App() {
  return (
    <div className="App">
      <BrowserRouter>  
        <Routes>
          <Route path="/" exact element={<Home/>} />
          <Route path="/about" exact element={<About />   } />      
          <Route path="/account" exact element={<Account />} />
          <Route path="/blog" exact element={<Blog />} />
          <Route path="/cart" exact element={<Cart />} />
          <Route path="/checkout" exact element={<Checkout />} />
          <Route path="/contact" exact element={<Contact />} />
          <Route path="/register/:special" element={<Register />} />
          <Route path="/login" exact element={<Login />} />
          <Route path="/logout" exact element={<Logout />} />
          <Route path="/shop" exact element={<Marketplace />} />
          <Route path="/listing/:listingid" exact element={<Listing />} />
          <Route path="/privacy" exact element={<Privacy />} />
          <Route path="/terms" exact element={<Tos />} />
          <Route path="/404" element={<FourOhFour />} />
          <Route render={() => <Navigate to= "/404" />}/>
        </Routes>
      </BrowserRouter>
  </div>
    
  );
}

export default App;
