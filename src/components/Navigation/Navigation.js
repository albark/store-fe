import React from 'react';
import cx from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Cookies from 'universal-cookie';
import s from './Navigation.css';
import Link from '../Link';

const cookies = new Cookies();

class Navigation extends React.Component {
  constructor(props) {
    super(props);
    this.handleLogout = this.handleLogout.bind(this);
  }

  async handleLogout(e) {
    e.preventDefault();
    const resp = await fetch('/graphql', {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        query: `mutation{userLogout()}`,
      }),
      credentials: 'include',
    });
    const data = await resp.json();
    cookies.remove('id_token');
    cookies.remove('user');
    cookies.remove('cart_owner_id');
    // history.push('/login');

    // this.setState({ password: e.target.value });
  }
  render() {
    let isLoggedIn = null;
    if (this.props.user && this.props.user.id) {
      isLoggedIn = true;
    } else {
      isLoggedIn = false;
    }
    return (
      <div className={s.root} role="navigation">
        {this.props.isShopMode &&
        <div>

        {isLoggedIn ? (
          <div>
          {/* <Link className={s.link} to="/about">
            About
          </Link> */}
          <Link className={s.link} to="/shop">
            Shop
          </Link>
          <Link className={s.link} to="/account">
          Account
          </Link>
          {/* <Link className={s.link} to="/cart">
            Cart
          </Link> */}
          <span className={s.spacer}> | </span>
          <Link className={s.link} to="/logout" onClick={this.handleLogout}>
            Log Out
          </Link>
          </div>
        ) : (
          <div>
          <Link className={s.link} to="/shop">
            shop
          </Link>
          <span className={s.spacer}> | </span>
            <Link className={cx(s.link, s.highlight)} to="/register">
              Sign up
            </Link>
            <span className={s.spacer}>or</span>
            <Link className={s.link} to="/login">
              Log In
            </Link>
          </div>
        )}
                  </div>}

                  {this.props.isSassMode &&
        <div>

        {isLoggedIn ? (
          <div>
          {/* <Link className={s.link} to="/about">
            About
          </Link> */}
          <Link className={s.link} to="/account">
          Account
          </Link>
          {/* <Link className={s.link} to="/cart">
            Cart
          </Link> */}
          <span className={s.spacer}> | </span>
          <Link className={s.link} to="/logout" onClick={this.handleLogout}>
            Log Out
          </Link>
          </div>
        ) : (
          <div>
            <Link className={s.link} to="/login">
              Log In
            </Link>
          </div>
        )}
                  </div>}

      </div>
    );
  }
}

export default withStyles(s)(Navigation);
