import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SaleItemCard.css';
import Link from '../Link';

export default function Footer () {
  return (
    <div className='SaleItemCard'>
    <Link className='link' to={`/listings?listing=${this.props.listing.id}`}>

        <div className='row itemCardImageRowContainer'>
          <div className='tweleve columns itemCardImageContainer'>
            <img src={`./user-uploaded/${this.props.listing.images[0]}.jpg`} className='itemCardImaged' />
          </div>
        </div>
        <div className='itemDataContainer'>
        <div className="row">
          <div className='tweleve columns'>
            <p className='itemCardTitle'> {this.props.listing.title}</p>
          </div>
        </div>
        <div className='row'>
          <div className='tweleve columns'>
            <p>${this.props.listing.price / 100}</p>
          </div>
        </div>

        <div className='row'>

          {this.props.deleteListing && (
            <div className='six columns'>
              <button
                onClick={this.props.deleteListing.bind(
                  this,
                  this.props.listing,
                )}
              >
                Delete Listing
              </button>
            </div>
          )}
        </div>
        </div>
      </Link>
    </div>
  )
}

export default withStyles(s)(SaleItemCard);
