import { Link } from 'react-router-dom';

export default function Footer () {
    return <div className="footerContainer">
        <div className="container actionContainer">
          <div className="row subTaglineTwo">
          <div className="four columns">
              <h4>
                Site
              </h4>
            <div>
              <a className="link" href="/shop">
              <p>Shop</p>
              </a>
              <a className="link" href="https://redbubble.com/people/ myceliadesigns/explore">
              <p>Redbubble</p>
              </a>
            </div>
            
            {/* {this.props.isBlogMode &&
              <Link className="link" to="/blog">
              <p>Blog</p>
              </Link>
            } */}

            </div>
            <div className="four columns">
            <h4>
            Company
            </h4>
            <Link className="link" to="/about">
              <p>About</p>
            </Link>
            <p className="copyrightBadge">© A2 Mushrooms LLC</p>
            </div>
            <div className="four columns">
            <h4>
            Resources
            </h4>
            <Link className="link" to="/contact">
              <p>Support</p>
            </Link>
            {/* <Link className={s.link} to="/contact">
              <p>Contact</p>
            </Link> */}
  
            <Link className="link" to="/terms">
              <p>Terms and Conditions</p>
            </Link>
  
            <Link className="link" to="/privacy">
              <p>Privacy Policy</p>
            </Link>
            </div>
          </div>
        </div>
      </div>
  }
  