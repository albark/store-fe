import { Link, useNavigate } from 'react-router-dom';
import Cookies from 'universal-cookie';
import getUser from '../helper/getUser'
import './styles.css';

function handleLogout(history) {
    let cookies = new Cookies()
  
    cookies.remove("user")
    cookies.remove("login_token")
    cookies.remove("cart")
    history.push('/login')
}

export default function Header () {
    let history = useNavigate()

    return (
        <div className="header">
            {getUser() ? (
                <div className="container">
                    <div className="navigation">
                        <Link className="link" to="/shop">
                            Shop
                        </Link>
                        {/* <Link className="link" to="/account">
                            Account
                        </Link>
                        <span className="spacer"> | </span>
                        <Link className="link" to="/logout" onClick={() => {handleLogout(history)}}>
                            Log Out
                        </Link> */}
                    </div>
                </div>
            ) : (
                <div className="container">
                    <div className="navigation">
                        <Link className="link" to="/shop">
                            shop
                        </Link>
                        {/* <span className="spacer"> | </span>
                        <Link className="link highlight" to="/register">
                            Sign up
                        </Link>
                        <span className="spacer">or</span>
                        <Link className="link" to="/login">
                            Log In
                        </Link> */}
                </div>
            </div>
            )}
        </div>
    )
}
